<?php

namespace App\Exports;

use App\Model\Data\Student;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StudentExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $student;

    public function __construct ($student) {
        $this->student = $student;
    }
    
    public function view(): View
    {
        $data['student']      =   $this->student;
        return view("client.teacherStudent.exportStudent", compact('data'));
    }
}
