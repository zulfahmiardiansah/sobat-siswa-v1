<?php

namespace App\Exports;

use App\Model\Attitude\Violation;
use App\Model\Data\Student;
use App\Model\Data\StudentClass;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ViolationExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $id;

    public function __construct ($id) {
        $this->id   =   $id;
    }
    
    public function view(): View
    {
        $id = $this->id;
        $violationRecord    =   \DB::select("SELECT
                                                    data_student.name,
                                                    data_student.gender as sex,
                                                    data_student.nis,
                                                    sum(attitude_rule.sanction) as total
                                            FROM
                                                data_student, attitude_rule, attitude_rule_violation
                                            WHERE 
                                                data_student.id = attitude_rule_violation.student_id
                                                AND attitude_rule.id = attitude_rule_violation.rule_id
                                                AND data_student.class_id = $id  
                                            GROUP BY data_student.name");
        foreach ($violationRecord as $single) {
            $data['violation']["$single->nis"]    =   $single->total;
        }
        $data['class']      =   StudentClass::find($id);
        $data['student']    =   Student::where("class_id", $id)->orderBy('name', 'ASC')->get();
        $data['index']      =   1;
        return view("client.attitude.teacherViolation.exportClass", compact('data'));
    }
}
