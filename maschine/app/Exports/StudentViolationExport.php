<?php

namespace App\Exports;

use App\Model\Data\Student;
use App\Model\Attitude\Violation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StudentViolationExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $id;

    public function __construct ($id) {
        $this->id   =   $id;
    }
    
    public function view(): View
    {
        $id = $this->id;
        $data['student']      =   Student::select("data_student.*", "data_class.name as class_name")
                                    ->join("data_class", "data_student.class_id", "data_class.id")
                                    ->where("data_student.id", $id)
                                    ->first();
        $data['violation']  =   Violation::select("attitude_rule_violation.*", "attitude_rule.code", "attitude_rule.description", "attitude_rule.sanction")
                                            ->join("attitude_rule", "attitude_rule_violation.rule_id", "attitude_rule.id")
                                            ->where("attitude_rule_violation.student_id", $id);
        $data['violationCount']     =   $data['violation']->sum("attitude_rule.sanction");
        $data['violation']          =   $data['violation']
                                            ->orderBy("attitude_rule_violation.date", "DESC")
                                            ->get();
        $data['index']              =   1;
        return view("client.attitude.teacherViolation.exportStudent", compact('data'));
    }
}
