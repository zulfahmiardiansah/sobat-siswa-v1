<?php

namespace App\Http\Middleware\Web;

use Closure;
use App\Http\Controllers\Client\AccountValidation;

class LevelStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validInf       =   AccountValidation::sessionValid($request->route()->parameter("code"), true);
        if ($validInf == false) {
            return \Redirect::to(route("student/login", $request->route()->parameter("code")));
        } 
        if (AccountValidation::expiredCheck($validInf)) {
            return \Redirect::to(route("expired"));
        }
        return $next($request);
    }
}
