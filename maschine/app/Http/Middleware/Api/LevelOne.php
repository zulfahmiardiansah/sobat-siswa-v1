<?php

namespace App\Http\Middleware\Api;

use Closure;
use App\Http\Controllers\Api\Auth;

class LevelOne
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authClass = new Auth();
        if (!$authClass->tokenValidation($request))  {
            return response([
                "message"   =>  "Invalid token"
            ], 401);
        } else {
            return $next($request);
        }
    }
}
