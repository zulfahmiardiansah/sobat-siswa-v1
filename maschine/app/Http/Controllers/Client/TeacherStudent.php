<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Data\Student;
use App\Model\Data\StudentClass;
use App\Exports\StudentExport;

class TeacherStudent extends Controller
{
    // Show Teacher Student Page
        public function showTeacherStudentPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['student']      =   Student::select("data_student.*", "data_class.name as class_name")
                                                    ->join("data_class", "data_student.class_id", "data_class.id")
                                                    ->where("data_class.school_id", $data['school']->id);
                if (isset($_GET['search'])) {
                    if (isset($_GET['keyword'])) {
                        $data['student']    =   $data['student']->where('data_student.name', 'LIKE', '%'. $_GET['keyword'] .'%');
                    }
                    if (isset($_GET['class_id'])) {
                        if ($_GET['class_id'] != "All") { 
                            $data['student']    =   $data['student']->where('data_student.class_id', $_GET['class_id']);
                        }
                    }
                    if (isset($_GET['gender'])) {
                        if ($_GET['gender'] != "All") { 
                            $data['student']    =   $data['student']->where('data_student.gender', $_GET['gender']);
                        }
                    }
                    $data['student']   =  $data['student']->orderBy("data_student.class_id", "ASC")
                                                    ->orderBy("data_student.name", "ASC")
                                                    ->get();
                } else {
                    $data['student']   =  $data['student']->orderBy("data_student.class_id", "ASC")
                                                    ->orderBy("data_student.name", "ASC")
                                                    ->paginate(15);
                }
                $data['studentTotal']  =   Student::where("school_id", $data['school']->id)->count();
                $data['class']         =    StudentClass::where("school_id", $data['school']->id)->orderBy('name', 'DESC')->get();
                return view("client.teacherStudent.showTeacherStudentPage", compact('data'));
        }

    // Show Edit Modal
        public function showEditModal (string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['class']          =   StudentClass::where("school_id", $data['school']->id)
                                                        ->orderBy("name", "ASC")
                                                        ->paginate(15);
                $data['student']        =   Student::find($id);
                $data['studentClass']   =   StudentClass::find($data['student']->class_id);
            // Return
                return view("client.teacherStudent.showEditModal", compact('data'));
        }

    // Show Add Modal
        public function showAddModal (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['class']      =   StudentClass::where("school_id", $data['school']->id)
                                                    ->orderBy("name", "ASC")
                                                    ->paginate(15);
            // Return
                return view("client.teacherStudent.showAddModal", compact('data'));
        }

    // Process Add
        public function processAdd (Request $request, string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Add
                $studentTotal           =   Student::where("school_id", $data['school']->id)->count();
                if ($studentTotal >= $data['school']->limit) {
                    return \Redirect::to(route("teacher/student", $data['school']->code))->with("error", "Your account has reached the limit");
                }
                $newRecord              =   new Student;
                $newRecord->nis         =   $request->get('nis');
                $newRecord->name        =   $request->get('name');
                $newRecord->email       =   $request->get('email');
                $newRecord->place_birth =   $request->get('place_birth');
                $newRecord->date_birth  =   $request->get('date_birth');
                $newRecord->address     =   $request->get('address');
                $newRecord->gender      =   $request->get('gender');
                $newRecord->phone       =   $request->get('phone');
                $newRecord->password    =   $request->get('password');

                if ($request->hasFile("picture")) {
                    $studentImage       =   $request->file('picture');
                    $extstudentImage    =   $studentImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extstudentImage;
                    $studentImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->picture =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                } else {
                    $newRecord->picture =   'public/adminAssets/img/default.png';
                }

                if (!StudentClass::find($request->get("class_id"))) {
                    return \Redirect::to(route("teacher/student", $data['school']->code))->with("error", "Class not found");
                }

                $newRecord->class_id    =   $request->get('class_id');
                $newRecord->school_id   =   $data['school']->id;
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/student", $data['school']->code))->with("success", "Add record success");
        }
    
    // Process Edit
        public function processEdit (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord              =   Student::find($id);
                $newRecord->nis         =   $request->get('nis');
                $newRecord->name        =   $request->get('name');
                $newRecord->email       =   $request->get('email');
                $newRecord->place_birth =   $request->get('place_birth');
                $newRecord->date_birth  =   $request->get('date_birth');
                $newRecord->address     =   $request->get('address');
                $newRecord->gender      =   $request->get('gender');
                $newRecord->phone       =   $request->get('phone');
                $newRecord->password    =   $request->get('password');

                if ($request->hasFile("picture")) {
                    $studentImage       =   $request->file('picture');
                    $extstudentImage    =   $studentImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extstudentImage;
                    $studentImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->picture =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }

                if (!StudentClass::find($request->get("class_id"))) {
                    return \Redirect::to(route("teacher/student", $data['school']->code))->with("error", "Class not found");
                }

                $newRecord->class_id    =   $request->get('class_id');
                $newRecord->school_id   =   $data['school']->id;
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/student", $data['school']->code))->with("success", "Edit record success");
        }

    // Process Delete
        public function processDelete (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Delete
                $oldRecord              =   Student::find($id);
                if ($oldRecord) $oldRecord->delete();
            // Return
                return \Redirect::to(route("teacher/student", $data['school']->code))->with("success", "Delete record success");
        }

        
    public function exportStudent (string $code)
    {
        // Check
            $data['school']     = AccountValidation::school($code, false);
            if ($data['school'] == false) {
                return \Redirect::to(route("student/login", $code));
            } 
            if (AccountValidation::expiredCheck($data['school'])) {
                return \Redirect::to(route("expired"));
            }
            $data['student']      =   Student::select("data_student.*", "data_class.name as class_name")
                                                ->join("data_class", "data_student.class_id", "data_class.id")
                                                ->where("data_class.school_id", $data['school']->id);
            if (isset($_GET['search'])) {
                if (isset($_GET['keyword'])) {
                    $data['student']    =   $data['student']->where('data_student.name', 'LIKE', '%'. $_GET['keyword'] .'%');
                }
                if (isset($_GET['class_id'])) {
                    if ($_GET['class_id'] != "All") { 
                        $data['student']    =   $data['student']->where('data_student.class_id', $_GET['class_id']);
                    }
                }
                if (isset($_GET['gender'])) {
                    if ($_GET['gender'] != "All") { 
                        $data['student']    =   $data['student']->where('data_student.gender', $_GET['gender']);
                    }
                }
                $data['student']   =  $data['student']->orderBy("data_student.class_id", "ASC")
                                                ->orderBy("data_student.name", "ASC")
                                                ->get();
            } else {
                $data['student']   =  $data['student']->orderBy("data_student.class_id", "ASC")
                                                ->orderBy("data_student.name", "ASC")
                                                ->get();
            }
            return \Excel::download(new StudentExport($data['student']), 'Student Recapitulation.xlsx');
    }
}
