<?php

namespace App\Http\Controllers\Client\Mading;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;

use App\Model\Mading\Content;

class TeacherManage extends Controller
{
    // Show Manage Page
        public function showManage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['content']      =   Content::where("school_id", $data['school']->id)
                                                    ->orderBy("created_at", "ASC")
                                                    ->paginate(15);
                return view("client.mading.teacherManage.showManage", compact('data'));
        }

        
    // Show Add Content
        public function showAddContent (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                return view("client.mading.teacherManage.showAddContent", compact('data'));
        }

    // Show Edit Content
        public function showEditContent (string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['content']    =   Content::find($id);
                return view("client.mading.teacherManage.showEditContent", compact('data'));
        }

    // Save Edit Content
        public function saveEditContent (string $code, Request $request, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord              =   Content::find($id);
                $newRecord->long_desc   =   $_POST['long_desc'];
                $newRecord->short_desc  =   $request->get('short_desc');
                $newRecord->type        =   $request->get('type');
                $newRecord->school_id   =   $data['school']->id;
                $newRecord->created_by  =   $_COOKIE['dc227fa48e4e89e835b0981eba3bd122'];
                if ($request->hasFile("thumbnail_img")) {
                    $uploadImage       =   $request->file('thumbnail_img');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->thumbnail_img =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                if ($request->hasFile("feature_img")) {
                    $uploadImage       =   $request->file('feature_img');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->feature_img =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                if ($request->hasFile("attachment")) {
                    $uploadImage       =   $request->file('attachment');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->attachment =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/mading/manage", $data['school']->code))->with("success", "Edit record success");
        }

    // Save Add Content
        public function saveAddContent (string $code, Request $request)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Add
                $newRecord              =   new Content;
                $newRecord->long_desc   =   $_POST['long_desc'];
                $newRecord->short_desc  =   $request->get('short_desc');
                $newRecord->type        =   $request->get('type');
                $newRecord->school_id   =   $data['school']->id;
                $newRecord->created_by  =   $_COOKIE['dc227fa48e4e89e835b0981eba3bd122'];
                if ($request->hasFile("thumbnail_img")) {
                    $uploadImage       =   $request->file('thumbnail_img');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->thumbnail_img =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                if ($request->hasFile("feature_img")) {
                    $uploadImage       =   $request->file('feature_img');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->feature_img =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                if ($request->hasFile("attachment")) {
                    $uploadImage       =   $request->file('attachment');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->attachment =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/mading/manage", $data['school']->code))->with("success", "Add record success");
        }

    // Process Delete
        public function processDelete (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Delete
                $oldRecord              =   Content::find($id);
                if ($oldRecord) $oldRecord->delete();
            // Return
                return \Redirect::to(route("teacher/mading/manage", $data['school']->code))->with("success", "Delete record success");
        }
}
