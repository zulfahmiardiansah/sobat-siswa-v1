<?php

namespace App\Http\Controllers\Client\Mading;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;

use App\Model\Mading\Content;
use App\Model\Mading\Comment;
use App\Model\Data\Student;

class StudentExplore extends Controller
{

    // Show Explore Page
        public function showExplore (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, true);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                if (isset($_GET['name'])) {
                    $data['content']      =   Content::where("school_id", $data['school']->id)
                                                        ->where('short_desc', 'LIKE', '%' . $_GET['name'] . '%')
                                                        ->orderBy("created_at", "DESC")
                                                        ->get();
                } else {
                    $data['content']      =   Content::where("school_id", $data['school']->id)
                                                        ->orderBy("created_at", "DESC")
                                                        ->paginate(6);
                }
                return view("client.mading.studentExplore.showExplore", compact('data'));
        }

    // Show Read Content
        public function showRead (string $code, int $id)
        {
            // Check
                $data['content']    =   Content::find($id);
                $data['school']     = AccountValidation::school($code, true);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if ($data['school']->id != $data['content']->school_id) {
                    return \Redirect::to(route("student/dashboard", $code));
                }
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['comment']    =   Comment::where("content_id", $id)->get();
                return view("client.mading.studentExplore.showRead", compact('data'));
        }

    // Save Comment
        public function saveComment (string $code, int $id, Request $request)
        {
            // Check
                $data['school']     = AccountValidation::school($code, true);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Data
                $Student                    =   Student::find($data['school']['session']->id);
                $newData                    =   new Comment;
                $newData->name              =   $_COOKIE['dc227fa48e4e89e835b0981eba3bd122'];
                $newData->comment           =   $request->get('comment');
                if ($Student->picture != null || $Student->picture != '') {
                    $newData->profile_picture   =   $Student->picture;
                } else {
                    $newData->profile_picture   =   'public/adminAssets/img/theme/team-4-800x800.jpg';
                }
                $newData->content_id        =   $id;
                $newData->save();
                $contentData                =   Content::find($id);
                $contentData->comment       =   $contentData->comment + 1;
                $contentData->save();
            // Return
                return \Redirect::to(route("student/mading/explore", $data['school']->code) . "/read/" . $id)->with("success", "Add comment success");
        }
}
