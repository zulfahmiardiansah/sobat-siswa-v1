<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\School;
use App\Model\Data\Teacher;
use App\Model\Data\StudentClass;
use App\Model\Data\Student;

class TeacherDashboard extends Controller
{
    // Show Teacher Dashboard Page
        public function showTeacherDashboardPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code);
            // Return
                $data['studentCount']       =   Student::where('school_id', $data['school']->id)->count();
                $data['classCount']         =   StudentClass::where('school_id', $data['school']->id)->count();
                $data['teacherCount']       =   Teacher::where('school_id', $data['school']->id)
                                                ->where("nip", "!=", "0")->count();
                $data                       =   $this->getWidgetData($data);
                return view("client.teacherDashboard.showTeacherDashboardPage", compact('data'));
        }
    
    // Get Widget Data
        public function getWidgetData ($data)
        {
            foreach (json_decode($data['school']->feature) as $single) {
                switch ($single->code) {
                    case 'attitude':
                        $data['recentViolation']    =   \DB::select("SELECT * FROM vw_violation WHERE school_id = " . $data['school']->id . '  ORDER BY CREATED_AT DESC LIMIT 5');
                        $data['recentAchievement']    =   \DB::select("SELECT * FROM vw_achievement WHERE school_id = " . $data['school']->id . '  ORDER BY CREATED_AT DESC LIMIT 5');
                        break;
                }
            }
            return $data;
        }
}
