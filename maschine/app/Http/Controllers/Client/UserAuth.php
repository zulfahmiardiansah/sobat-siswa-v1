<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\School;
use App\Model\Data\Teacher;
use App\Model\Data\Student;
use App\Model\Utility\SessionGetter;

class UserAuth extends Controller
{
    // Show Login Page
        public function showLoginPage (string $code)
        {
            // School
                $data['school']   =   School::where('code', $code)->first();
                if ($data['school'] == false) {
                    return \Redirect::to(url("/"));
                }
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                return view("client.userAuth.showLoginPage", compact('data'));
        }

    // Show Teacher Login Page
        public function showTeacherLoginPage (string $code)
        {
            // School
                $data['school']   =   School::where('code', $code)->first();
                if ($data['school'] == false) {
                    return \Redirect::to(url("/"));
                }
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                return view("client.userAuth.showTeacherLoginPage", compact('data'));
        }
    
    // Process Teacher
        public function processTeacher (Request $request, string $code)
        {
            // School
                $data['school']     =   School::where('code', $code)->first();
                if ($data['school'] == false) {
                    return \Redirect::to(url("/"));
                }
            // Login
                $teacherRecord      =   Teacher::where("username", $request->get("username"))
                                                ->where("school_id", $data['school']->id);
                if ($teacherRecord->count() == 0) {
                    $teacherRecord      =   Teacher::where("email", $request->get("username"))
                                                    ->where("school_id", $data['school']->id);
                    if ($teacherRecord->count() == 0) {
                        return \Redirect::to(route("teacher/login", $code))->with("error", "Unknown account");
                    }
                }
                $teacherRecord  =   $teacherRecord->first();
                if (\Hash::check($request->get('password'), $teacherRecord->password)) {
                    $sessionKey             =   bcrypt(rand(0000000000, 999999999));
                    $sessionValue["role"]   =   $teacherRecord->role;
                    $sessionValue["name"]   =   $teacherRecord->name;
                    $sessionValue["id"]     =   $teacherRecord->id; 
                    $sessionValue["code"]   =   $code;
                    $sessionRecord          =   new SessionGetter();
                    $sessionRecord->session_value   =   json_encode($sessionValue);
                    $sessionRecord->session_key     =   $sessionKey;
                    $sessionRecord->save();
                    setcookie("0f9297fbfd4b82c603bf5d026093c3a4", $sessionKey, 0,  "/");
                    return \Redirect::to(route("teacher/dashboard", $code))->with("success", "Login success");
                }
                return \Redirect::to(route("teacher/login", $code))->with("error", "Unknown account");
        }

    // Process Student
        public function processStudent (Request $request, string $code)
        {
            // School
                $data['school']     =   School::where('code', $code)->first();
                if ($data['school'] == false) {
                    return \Redirect::to(url("/"));
                }
            // Login
                $studentRecord      =   Student::where("nis", $request->get("nis"))
                                                ->where("school_id", $data['school']->id);
                if (!$studentRecord->count()) {
                    return \Redirect::to(route("student/login", $code))->with("error", "Unknown account");
                }
                $studentRecord  =   $studentRecord->first();
                if ($request->get('password') == $studentRecord->password) {
                    $sessionKey             =   bcrypt(rand(0000000000, 999999999));
                    $sessionValue["role"]   =   3;
                    $sessionValue["name"]   =   $studentRecord->name;
                    $sessionValue["id"]     =   $studentRecord->id; 
                    $sessionValue["code"]   =   $code;
                    if ($studentRecord->picture) {
                        $sessionValue["profileImg"] =   $studentRecord->picture;
                    }
                    $sessionRecord          =   new SessionGetter();
                    $sessionRecord->session_value   =   json_encode($sessionValue);
                    $sessionRecord->session_key     =   $sessionKey;
                    $sessionRecord->save();
                    setcookie("0f9297fbfd4b82c603bf5d026093c3a4", $sessionKey, 0,  "/");
                    return \Redirect::to(route("student/dashboard", $code))->with("success", "Login success");
                }
                    return \Redirect::to(route("student/login", $code))->with("error", "Unknown account");
        }

    // Process Logout
        public function processLogout (string $code)
        {
            SessionGetter::where("session_key", $_COOKIE['0f9297fbfd4b82c603bf5d026093c3a4'])->delete();
            setcookie("0f9297fbfd4b82c603bf5d026093c3a4", false, 0,  "/");
            return \Redirect::to(route("student/login", $code));
        }
    
    
}
