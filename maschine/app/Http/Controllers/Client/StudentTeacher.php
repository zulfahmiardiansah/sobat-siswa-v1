<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Data\Teacher;

class StudentTeacher extends Controller
{
    // showStudentTeacherPage
        public function showStudentTeacherPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, true);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['teacher']       =   Teacher::where('school_id', $data['school']->id)
                                                    ->where("nip", "!=", "0");
                if (isset($_GET['name'])) {
                    $data['teacher']    =   $data['teacher']->where('name', 'LIKE', '%' . $_GET['name'] . '%')
                                                            ->orderBy("name", "ASC")
                                                            ->get();
                } else {
                    $data['teacher']    =   $data['teacher']->orderBy("name", "ASC")
                                                            ->paginate(8);
                }
                return view("client.studentTeacher.showStudentTeacherPage", compact('data'));
        }
}
