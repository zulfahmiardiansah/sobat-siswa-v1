<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\School;
use App\Model\Utility\SessionGetter;

class AccountValidation extends Controller
{
    public static function school (string $code) {
        $sessionRecord  =   SessionGetter::where("session_key", $_COOKIE['0f9297fbfd4b82c603bf5d026093c3a4'])->first();
        $result         =   School::select("main_school.*", "main_plan.name as plan_name", "main_plan.feature", "main_plan.limit")
                                    ->join("main_plan", "main_school.plan_id", "main_plan.id")
                                    ->where('code', $code)
                                    ->first();
        $session        =   json_decode($sessionRecord->session_value);
        $result["session"] = $session;
        return $result;
    }

    public static function sessionValid (string $code, bool $student = true) {
        if (isset($_COOKIE['0f9297fbfd4b82c603bf5d026093c3a4'])) {
            $sessionRecord = SessionGetter::where("session_key", $_COOKIE['0f9297fbfd4b82c603bf5d026093c3a4'])->first();
            if ($sessionRecord) {
                $session = json_decode($sessionRecord->session_value);
                if ($session->code == $code) {
                    $result   =   School::select("main_school.*", "main_plan.name as plan_name", "main_plan.feature", "main_plan.limit")
                                    ->join("main_plan", "main_school.plan_id", "main_plan.id")
                                    ->where('code', $code)
                                    ->first();
                    if ($result) {
                        if ($student) {
                            return $result;
                        } else {
                            if ($session->role < 3) {
                                return $result;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function expiredCheck ($data) {
        $nowDay         =   date("Y-m-d");
        if ($nowDay < $data->lifetime) {
            return false;
        } else {
            return true;
        }
    }
}
