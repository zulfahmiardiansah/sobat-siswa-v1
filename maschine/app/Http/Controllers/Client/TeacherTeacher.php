<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Data\Teacher;

class TeacherTeacher extends Controller
{
    // Show Teacher Page
        public function showTeacherPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['teacher']       =   Teacher::where('school_id', $data['school']->id)
                                                    ->where("nip", "!=", "0")
                                                    ->orderBy("name", "ASC")
                                                    ->paginate(15);
                return view("client.teacherTeacher.showTeacherPage", compact('data'));
        }

    // Show Add Modal
        public function showAddModal (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                return view("client.teacherTeacher.showAddModal", compact('data'));
        }

    // Show Edit Modal
        public function showEditModal (string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['teacher']    =   Teacher::find($id);
                return view("client.teacherTeacher.showEditModal", compact('data'));
        }

    // Process Edit
        public function processEdit (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord              =   Teacher::find($id);
                $newRecord->nip         =   $request->get('nip');
                $newRecord->name        =   $request->get('name');
                $newRecord->email       =   $request->get('email');
                $newRecord->gender      =   $request->get('gender');
                $newRecord->phone       =   $request->get('phone');
                $newRecord->username    =   $request->get('username');
                if ($request->get('password')){
                    $newRecord->password    =   \Hash::make($request->get('password'));
                }
                if ($request->hasFile("picture")) {
                    $teacherImage       =   $request->file('picture');
                    $extteacherImage    =   $teacherImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extteacherImage;
                    $teacherImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->picture =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                $newRecord->role        =   $request->get('role');
                $newRecord->school_id   =   $data['school']->id;
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/teacher", $data['school']->code))->with("success", "Edit record success");
        }

    // Process Add
        public function processAdd (Request $request, string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Add
                $newRecord              =   new Teacher;
                $newRecord->nip         =   $request->get('nip');
                $newRecord->name        =   $request->get('name');
                $newRecord->email       =   $request->get('email');
                $newRecord->gender      =   $request->get('gender');
                $newRecord->phone       =   $request->get('phone');
                $newRecord->username    =   $request->get('username');
                $newRecord->password    =   \Hash::make($request->get('password'));
                $newRecord->role        =   $request->get('role');
                $newRecord->school_id   =   $data['school']->id;
                if ($request->hasFile("picture")) {
                    $teacherImage       =   $request->file('picture');
                    $extteacherImage    =   $teacherImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extteacherImage;
                    $teacherImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->picture =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                } else {
                    $newRecord->picture =   'public/adminAssets/img/default.png';
                }
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/teacher", $data['school']->code))->with("success", "Add record success");
        }

    // Process Delete
        public function processDelete (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Delete
                $oldRecord              =   Teacher::find($id);
                if ($oldRecord) $oldRecord->delete();
            // Return
                return \Redirect::to(route("teacher/teacher", $data['school']->code))->with("success", "Delete record success");
        }
}
