<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\School;
use App\Model\Data\Teacher;
use App\Model\Data\StudentClass;
use App\Model\Data\Student;

class TeacherInformation extends Controller
{
    // Show Information Page
        public function showInformationPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['studentCount']       =   Student::where('school_id', $data['school']->id)->count();
                $data['classCount']         =   StudentClass::where('school_id', $data['school']->id)->count();
                $data['teacherCount']       =   Teacher::where('school_id', $data['school']->id)
                                                ->where("nip", "!=", "0")->count();
                return view("client.teacherInformation.showInformationPage", compact('data'));
        }

    // Process Edit
        public function processEdit (Request $request, string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord              =   School::find($data['school']->id);
                if ($newRecord) {
                    $newRecord->name         =   $request->get('name');
                    $newRecord->phone        =   $request->get('phone');
                    $newRecord->email        =   $request->get('email');
                    $newRecord->address      =   $request->get('address');
                    $validation             =   \Validator::make($request->all(), [
                                                    "logo" => "required|image"
                                                ]);
                    if (!$validation->fails()) {
                        $logoImage              =   $request->file('logo');
                        $extlogoImage           =   $logoImage->getClientOriginalExtension();
                        $newName                =   rand(1111111,9999999) . '.' . $extlogoImage;
                        $logoImage->move('public/upload/school/logo/', $newName);
                        $newRecord->logo        =   'public/upload/school/logo/' . $newName;
                    }
                    $newRecord->save();
                }
            // Return
                return \Redirect::to(route("teacher/information", $data['school']->code))->with("success", "Edit record success");
        }
}
