<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Data\StudentClass;
use App\Model\Data\Student;

class TeacherClass extends Controller
{
    // Show Teacher Class Page
        public function showTeacherClassPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['class']      =   StudentClass::where("school_id", $data['school']->id)
                                                    ->orderBy("name", "ASC")
                                                    ->paginate(15);
                return view("client.teacherClass.showTeacherClassPage", compact('data'));
        }

    // Show Edit Modal
        public function showEditModal (string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['class']      =   StudentClass::find($id);
                return view("client.teacherClass.showEditModal", compact('data'));
        }

    // Process Add
        public function processAdd (Request $request, string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Add
                $newRecord              =   new StudentClass;
                $newRecord->name        =   $request->get('name');
                $newRecord->school_id   =   $data['school']->id;
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/class", $data['school']->code))->with("success", "Add record success");
        }
    
    // Process Edit
        public function processEdit (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord              =   StudentClass::find($id);
                if ($newRecord) {
                    $newRecord->name        =   $request->get('name');
                    $newRecord->save();
                }
            // Return
                return \Redirect::to(route("teacher/class", $data['school']->code))->with("success", "Edit record success");
        }
    
    // Process Delete
        public function processDelete (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Delete
                $oldRecord              =   StudentClass::find($id);
                if ($oldRecord) $oldRecord->delete();
                Student::where('class_id', $id)->delete();
            // Return
                return \Redirect::to(route("teacher/class", $data['school']->code))->with("success", "Delete record success");
        }
}
