<?php

namespace App\Http\Controllers\Client\Attitude;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;

class TeacherStatistics extends Controller
{    // Show Page
    public function showPage (string $code)
    {
        // Check
            $data['school']     = AccountValidation::school($code, false);
            if ($data['school'] == false) {
                return \Redirect::to(route("student/login", $code));
            } 
            if (AccountValidation::expiredCheck($data['school'])) {
                return \Redirect::to(route("expired"));
            }
        // Return
            $schoolId                   =   $data['school']->id;
            if (isset($_GET['date'])) {
                $dateExpl               =   explode("/", $_GET['date']);
                if (count($dateExpl) == 2) {
                    $dateSearch             =   "'" . $dateExpl[1] . "-" . $dateExpl[0] . "-01'";
                    $dateGraph              =   date('Y-m', strtotime($dateExpl[1] . "-" . $dateExpl[0] . "-01"));
                } else {
                    unset($_GET['date']);
                    $dateSearch             =   "NOW()";
                    $dateGraph              =   date('Y-m');          
                }
            } else {
                $dateSearch             =   "NOW()";
                $dateGraph              =   date('Y-m');                
            }
            $data['monthTitle']         =   date("F Y", strtotime($dateGraph));
            $data['violationRank']      =   \DB::select("SELECT DISTINCT
                                                            attitude_rule.code,
                                                            attitude_rule.description,
                                                            COUNT( attitude_rule.code ) AS total
                                                        FROM
                                                            attitude_rule,
                                                            attitude_rule_violation,
                                                            data_student
                                                        WHERE 
                                                            MONTH ( attitude_rule_violation.date ) = MONTH(" . $dateSearch . ") 
                                                            AND YEAR ( attitude_rule_violation.date ) = YEAR(" . $dateSearch . ")
                                                            AND attitude_rule_violation.rule_id = attitude_rule.id
                                                            AND data_student.id = attitude_rule_violation.student_id
                                                            AND data_student.school_id = $schoolId
                                                        GROUP BY
                                                            attitude_rule.code 
                                                        ORDER BY
                                                            total DESC 
                                                            LIMIT 5");
            for ($i=-4; $i <= 0; $i++) { 
                $monthStats                     =   date('Y-m-', strtotime($dateGraph ." +".$i." month")).'1';
                $query                          =   "SELECT
                                                        count( attitude_rule_violation.id ) AS total,
                                                        DATE( '$monthStats' ) AS date_test 
                                                    FROM
                                                        attitude_rule_violation,
                                                        data_student
                                                    WHERE
                                                        MONTH ( attitude_rule_violation.date ) = MONTH (
                                                        '$monthStats') 
                                                        AND YEAR ( attitude_rule_violation.date ) = YEAR (
                                                        '$monthStats')
                                                        AND attitude_rule_violation.student_id = data_student.id
                                                        AND data_student.school_id = $schoolId";
                $data['violationGraph'][]     =    \DB::select($query);
            }
            $data['violationClassTable']        =   \DB::select("SELECT DISTINCT
                                                                    data_class.name,
                                                                    COUNT( data_class.name ) AS total 
                                                                FROM
                                                                    data_class,
                                                                    data_student,
                                                                    attitude_rule_violation
                                                                WHERE
                                                                    attitude_rule_violation.student_id = data_student.id
                                                                    AND data_student.class_id = data_class.id
                                                                    AND MONTH ( attitude_rule_violation.date ) = MONTH(" . $dateSearch . ") 
                                                                    AND YEAR ( attitude_rule_violation.date ) = YEAR(" . $dateSearch . ") 
                                                                    AND data_student.school_id = $schoolId
                                                                GROUP BY
                                                                    data_class.name 
                                                                ORDER BY
                                                                    total DESC 
                                                                    LIMIT 5");

            $achievementRegionCategory          =   ['Wilayah', 'Kota', 'Provinsi', 'Nasional', 'Internasional', 'Tidak Berkategori'];
            $query                              =   "SELECT
                                                        attitude_achievement.area,
                                                        count( attitude_achievement.id ) AS total 
                                                    FROM
                                                        attitude_achievement
                                                        JOIN data_student ON attitude_achievement.student_id = data_student.id 
                                                    WHERE
                                                        data_student.school_id = " . $schoolId . " 
                                                    AND MONTH ( attitude_achievement.date ) = MONTH(" . $dateSearch . ") 
                                                    AND YEAR ( attitude_achievement.date ) = YEAR(" . $dateSearch . ") 
                                                    GROUP BY
                                                        area";
            foreach ($achievementRegionCategory as $single) {
                $data['achievementRegion'][]     =   \DB::select("SELECT * FROM ($query) as a WHERE a.area = '$single'");
            }

            $achievementTypeCategory          =   ['Akademis', 'Non-Akademis', 'Tidak Berkategori'];
            $query                              =   "SELECT
                                                        attitude_achievement.type,
                                                        count( attitude_achievement.id ) AS total 
                                                    FROM
                                                        attitude_achievement
                                                        JOIN data_student ON attitude_achievement.student_id = data_student.id 
                                                    WHERE
                                                        data_student.school_id = " . $schoolId . " 
                                                    AND MONTH ( attitude_achievement.date ) = MONTH(" . $dateSearch . ") 
                                                    AND YEAR ( attitude_achievement.date ) = YEAR(" . $dateSearch . ") 
                                                    GROUP BY
                                                        type";
            foreach ($achievementTypeCategory as $single) {
                $data['achievementType'][]     =   \DB::select("SELECT * FROM ($query) as a WHERE a.type = '$single'");
            }

            return view("client.attitude.teacherStatistics.showPage", compact('data'));
    }
}
