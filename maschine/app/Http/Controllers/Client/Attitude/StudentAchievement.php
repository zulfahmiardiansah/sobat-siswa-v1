<?php

namespace App\Http\Controllers\Client\Attitude;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;

use App\Model\Data\StudentClass;
use App\Model\Data\Student;
use App\Model\Attitude\Achievement;

class StudentAchievement extends Controller
{
    // Get Page  
    public function getPage (string $code)
        {
            $data['school']     = AccountValidation::school($code);
            // Return
                $data['student']      =   Student::select("data_student.*", "data_class.name as class_name")
                                            ->join("data_class", "data_student.class_id", "data_class.id")
                                            ->where("data_student.id", $data['school']['session']->id)
                                            ->first();
                $data['achievement']  =   Achievement::where("student_id", $data['school']['session']->id)
                                                    ->orderBy("date", "DESC")
                                                    ->paginate(15);
                return view("client.attitude.studentAchievement.getPage", compact('data'));
        }
}
