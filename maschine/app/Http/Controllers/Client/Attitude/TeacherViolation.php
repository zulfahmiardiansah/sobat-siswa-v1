<?php

namespace App\Http\Controllers\Client\Attitude;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;
use Illuminate\Pagination\Paginator;

use App\Model\Data\StudentClass;
use App\Model\Data\Student;
use App\Model\Data\Token;
use App\Model\Attitude\Violation;
use App\Model\Attitude\Rule;

use App\Exports\ViolationExport;
use App\Exports\StudentViolationExport;

class TeacherViolation extends Controller
{
    // Show Select Student
        public function showSelectStudentPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['class']      =   StudentClass::where("school_id", $data['school']->id)
                                                    ->orderBy("name", "ASC")
                                                    ->paginate(15);
                return view("client.attitude.teacherViolation.showSelectStudentPage", compact('data'));
        }

    // Get Student
        public function getStudent (string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['student']    =   Student::where("school_id", $data['school']->id)
                                                ->where("class_id", $id)
                                                ->orderBy('name', 'ASC')
                                                ->get();
                $data['id']         =   $id;
                return view("client.attitude.teacherViolation.getStudent", compact('data'));
        }

    // Show Add Modal
        public function showAddModal (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['studentId']  =   $id;
                $data['rule']       =   Rule::where("school_id", $data['school']->id)
                                            ->orderBy("code", "ASC")
                                            ->get();
                return view("client.attitude.teacherViolation.showAddModal", compact('data'));
        }

    // Show Edit Modal
        public function showEditModal (Request $request, string $code, int $id, int $idR)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['studentId']  =   $id;
                $data['rule']       =   Rule::where("school_id", $data['school']->id)
                                            ->orderBy("code", "ASC")
                                            ->get();
                $data['violation']  =   Violation::find($idR);
                $data['ruleSingle'] =   Rule::find($data['violation']->rule_id);
                return view("client.attitude.teacherViolation.showEditModal", compact('data'));
        }

    // Get Single  
        public function getSingle (string $code, int $id)
        {
            // Check
                $data['student']      =   Student::select("data_student.*", "data_class.name as class_name")
                                            ->join("data_class", "data_student.class_id", "data_class.id")
                                            ->where("data_student.id", $id)
                                            ->first();
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } else if ($data['student']->school_id != $data['school']->id) {
                    return \Redirect::to(route("teacher/dashboard", $data['school']->code));
                }
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
                if (!Student::find($id)) {
                    return \Redirect::to(route("teacher/dashboard", $data['school']->code));
                }
            // Return
                $data['violation']  =   Violation::select("attitude_rule_violation.*", "attitude_rule.code", "attitude_rule.description", "attitude_rule.sanction")
                                                    ->join("attitude_rule", "attitude_rule_violation.rule_id", "attitude_rule.id")
                                                    ->where("attitude_rule_violation.student_id", $id);
                $data['violationCount']     =   $data['violation']->sum("attitude_rule.sanction");
                $data['violation']          =   $data['violation']
                                                    ->orderBy("attitude_rule_violation.date", "DESC")
                                                    ->paginate(15);
                return view("client.attitude.teacherViolation.showSingle", compact('data'));
        }

    // Process Edit
        public function processEdit (Request $request, string $code, int $id, int $idR)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord                  =   Violation::find($idR);
                $newRecord->date            =   $request->get('date');
                $newRecord->rule_id     =   $request->get('rule_id');
                $newRecord->student_id   =   $id;
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/attitude/violation", $data['school']->code) . '/single/' . $id )->with("success", "Edit record success");
        }

    // Process Add
        public function processAdd (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Add
                $newRecord                  =   new Violation;
                $newRecord->date            =   $request->get('date');
                $newRecord->rule_id         =   $request->get('rule_id');
                $newRecord->student_id      =   $id;
                $newRecord->save();
            // Line Notification
                $tokenRecord                =   Token::where("account_id", $id)->where("role", 2)->where("type", "Line");
                if ($tokenRecord->count() != 0) {
                    $ruleRecord             =   Rule::find($request->get("rule_id"));
                    $tokenRecord            =   $tokenRecord->first();
                    $message                =   "💢 Oops, kamu diberikan " . $ruleRecord->sanction . " poin pelanggaran karena melanggar : " . $ruleRecord->description . " pada " . date("d F Y", strtotime($request->get('date'))) . ". 📢 Untuk lebih lengkapnya, cek di aplikasi Sobat Siswa-mu ya ! cip cip cip.";
                    $source                 =   $tokenRecord->token;
                    $curl                   =   curl_init();
                    curl_setopt_array($curl, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => "https://line.sobatsiswa.id/notification",
                        CURLOPT_POST => 1,
                        CURLOPT_POSTFIELDS => [
                            "to" => $source,
                            "text" => $message
                        ]
                    ]);
                    $resp = curl_exec($curl);
                    curl_close($curl);
                }
            // Return
                return \Redirect::to(route("teacher/attitude/violation", $data['school']->code) . '/single/' . $id )->with("success", "Add record success");
        }

    // Process Delete
        public function processDelete (Request $request, string $code, int $id, int $idR)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Delete
                $oldRecord              =   Violation::find($idR);
                if ($oldRecord) $oldRecord->delete();
            // Return
                return \Redirect::to(route("teacher/attitude/violation", $data['school']->code) . '/single/' . $id )->with("success", "Delete record success");
        }

    // Export
        public function showClass (string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $violationRecord    =   \DB::select("SELECT
                                                            data_student.name,
                                                            data_student.gender as sex,
                                                            data_student.nis,
                                                            sum(attitude_rule.sanction) as total
                                                    FROM
                                                        data_student, attitude_rule, attitude_rule_violation
                                                    WHERE 
                                                        data_student.id = attitude_rule_violation.student_id
                                                        AND attitude_rule.id = attitude_rule_violation.rule_id
                                                        AND data_student.class_id = $id  
                                                    GROUP BY data_student.name");
                foreach ($violationRecord as $single) {
                    $data['violation']["$single->nis"]    =   $single->total;
                }
                $data['class']      =   StudentClass::find($id);
                if (!$data['class']) {
                    return \Redirect::to(route("teacher/attitude/violation", $code));
                }
                $data['student']    =   Student::where("class_id", $id)->orderBy('name', 'ASC')->get();
                return view("client.attitude.teacherViolation.showClass", compact('data'));
        }
    
    public function exportClass (string $code, int $id)
    {
        $data['class']      =   StudentClass::find($id);
        return \Excel::download(new ViolationExport($id), 'Rule Violation Recapitulation ' . $data['class']->name . '.xlsx');
    }

    public function exportStudentViolation (string $code, int $id)
    {
        $data['student']    =   Student::find($id);
        return \Excel::download(new StudentViolationExport($id), $data['student']->name  .  '.xlsx');
    }
}
