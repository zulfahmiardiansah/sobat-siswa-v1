<?php

namespace App\Http\Controllers\Client\Attitude;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;

use App\Model\Data\StudentClass;
use App\Model\Data\Student;
use App\Model\Attitude\Achievement;

class TeacherAchievement extends Controller
{
    // Show Select Student
        public function showSelectStudentPage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['class']      =   StudentClass::where("school_id", $data['school']->id)
                                                    ->orderBy("name", "ASC")
                                                    ->paginate(15);
                return view("client.attitude.teacherAchievement.showSelectStudentPage", compact('data'));
        }

    // Get Student
        public function getStudent (string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['student']    =   Student::where("school_id", $data['school']->id)
                                                ->where("class_id", $id)
                                                ->orderBy('name', 'ASC')
                                                ->get();
                return view("client.attitude.teacherAchievement.getStudent", compact('data'));
        }

    // Show Add Modal
        public function showAddModal (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['studentId']  =   $id;
                return view("client.attitude.teacherAchievement.showAddModal", compact('data'));
        }

    // Show Edit Modal
        public function showEditModal (Request $request, string $code, int $id, int $idR)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['studentId']      =   $id;
                $data['achievement']    =   Achievement::find($idR);
                return view("client.attitude.teacherAchievement.showEditModal", compact('data'));
        }

    // Get Single  
        public function getSingle (string $code, int $id)
        {
            // Check
                $data['student']      =   Student::select("data_student.*", "data_class.name as class_name")
                                            ->join("data_class", "data_student.class_id", "data_class.id")
                                            ->where("data_student.id", $id)
                                            ->first();
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                }  else if ($data['student']->school_id != $data['school']->id) {
                    return \Redirect::to(route("teacher/dashboard", $data['school']->code));
                }
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
                if (!Student::find($id)) {
                    return \Redirect::to(route("teacher/dashboard", $data['school']->code));
                }
            // Return
                $data['achievement']  =   Achievement::where("student_id", $id)
                                                    ->orderBy("date", "DESC")
                                                    ->paginate(15);
                return view("client.attitude.teacherAchievement.showSingle", compact('data'));
        }

    // Process Edit
        public function processEdit (Request $request, string $code, int $id, int $idR)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord                  =   Achievement::find($idR);
                $newRecord->name            =   $request->get('name');
                $newRecord->description     =   $request->get('description');
                $newRecord->date     =   $request->get('date');
                $newRecord->area     =   $request->get('area');
                $newRecord->type     =   $request->get('type');
                $newRecord->organizer     =   $request->get('organizer');
                $newRecord->student_id   =   $id;
                if ($request->hasFile("certificate")) {
                    $uploadImage       =   $request->file('certificate');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->certificate =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/attitude/achievement", $data['school']->code) . '/single/' . $id )->with("success", "Edit record success");
        }

    // Process Add
        public function processAdd (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Add
                $newRecord                  =   new Achievement;
                $newRecord->name            =   $request->get('name');
                $newRecord->description     =   $request->get('description');
                $newRecord->date     =   $request->get('date');
                $newRecord->area     =   $request->get('area');
                $newRecord->type     =   $request->get('type');
                $newRecord->organizer     =   $request->get('organizer');
                $newRecord->student_id   =   $id;
                if ($request->hasFile("certificate")) {
                    $uploadImage       =   $request->file('certificate');
                    $extuploadImage    =   $uploadImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extuploadImage;
                    $uploadImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->certificate =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                }
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/attitude/achievement", $data['school']->code) . '/single/' . $id )->with("success", "Add record success");
        }

    // Process Delete
        public function processDelete (Request $request, string $code, int $id, int $idR)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Delete
                $oldRecord              =   Achievement::find($idR);
                if ($oldRecord) $oldRecord->delete();
            // Return
                return \Redirect::to(route("teacher/attitude/achievement", $data['school']->code) . '/single/' . $id )->with("success", "Delete record success");
        }
}
