<?php

namespace App\Http\Controllers\Client\Attitude;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;

use App\Model\Attitude\Rule;

class TeacherRule extends Controller
{
    // Show Teacher Rule Page
        public function showTeacherRulePage (string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['rule']       =   Rule::where("school_id", $data['school']->id)
                                            ->orderBy("code", "ASC")
                                            ->paginate(15);
                return view("client.attitude.teacherRule.showTeacherRulePage", compact('data'));
        }

    // Show Add Modal
        public function showAddModal (Request $request, string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                return view("client.attitude.teacherRule.showAddModal", compact('data'));
        }

    // Show Edit Modal
        public function showEditModal (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Return
                $data['rule']       =   Rule::find($id);
                return view("client.attitude.teacherRule.showEditModal", compact('data'));
        }

    // Process Add
        public function processAdd (Request $request, string $code)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Add
                $newRecord                  =   new Rule;
                $newRecord->code            =   $request->get('code');
                $newRecord->description     =   $request->get('description');
                $newRecord->sanction        =   $request->get('sanction');
                $newRecord->school_id   =   $data['school']->id;
                $newRecord->save();
            // Return
                return \Redirect::to(route("teacher/attitude/rule", $data['school']->code))->with("success", "Add record success");
        }

    // Process Edit
        public function processEdit (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Edit
                $newRecord                  =   Rule::find($id);
                if ($newRecord) {
                    $newRecord->code            =   $request->get('code');
                    $newRecord->description     =   $request->get('description');
                    $newRecord->sanction        =   $request->get('sanction');
                    $newRecord->school_id   =   $data['school']->id;
                    $newRecord->save();
                }
            // Return
                return \Redirect::to(route("teacher/attitude/rule", $data['school']->code))->with("success", "Edit record success");
        }

    // Process Delete
        public function processDelete (Request $request, string $code, int $id)
        {
            // Check
                $data['school']     = AccountValidation::school($code, false);
                if ($data['school'] == false) {
                    return \Redirect::to(route("student/login", $code));
                } 
                if (AccountValidation::expiredCheck($data['school'])) {
                    return \Redirect::to(route("expired"));
                }
            // Delete
                $oldRecord              =   Rule::find($id);
                if ($oldRecord) $oldRecord->delete();
            // Return
                return \Redirect::to(route("teacher/attitude/rule", $data['school']->code))->with("success", "Delete record success");
        }
}
