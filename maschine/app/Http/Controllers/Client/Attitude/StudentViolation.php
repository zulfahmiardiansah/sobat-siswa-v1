<?php

namespace App\Http\Controllers\Client\Attitude;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;
use Illuminate\Pagination\Paginator;

use App\Model\Data\Student;
use App\Model\Attitude\Violation;

use App\Exports\StudentViolationExport;

class StudentViolation extends Controller
{
    // Get Page  
        public function getPage (string $code)
        {
            $data['school']     = AccountValidation::school($code);
            // Return
                $data['student']      =   Student::select("data_student.*", "data_class.name as class_name")
                                            ->join("data_class", "data_student.class_id", "data_class.id")
                                            ->where("data_student.id", $data['school']['session']->id)
                                            ->first();
                $data['violation']  =   Violation::select("attitude_rule_violation.*", "attitude_rule.code", "attitude_rule.description", "attitude_rule.sanction")
                                                    ->join("attitude_rule", "attitude_rule_violation.rule_id", "attitude_rule.id")
                                                    ->where("attitude_rule_violation.student_id", $data['school']['session']->id);
                $data['violationCount']     =   $data['violation']->sum("attitude_rule.sanction");
                $data['violation']          =   $data['violation']
                                                    ->orderBy("attitude_rule_violation.date", "DESC")
                                                    ->paginate(15);
                return view("client.attitude.studentViolation.getPage", compact('data'));
        }

        
    public function exportStudentViolation (string $code)
    {
        $data['student']    =   Student::find($data['school']['session']->id);
        return \Excel::download(new StudentViolationExport($data['school']['session']->id), $data['student']->name  .  '.xlsx');
    }
}
