<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\School;
use App\Model\Data\Teacher;
use App\Model\Data\StudentClass;
use App\Model\Data\Student;

class StudentDashboard extends Controller
{
    // showStudentDashboardPage
        public function showStudentDashboardPage (string $code)
        {
            $data['school']     = AccountValidation::school($code);
            // 
                $data['biodata']            =   Student::find($data['school']['session']->id);
                $data['studentCount']       =   Student::where('school_id', $data['school']->id)->count();
                $data['classCount']         =   StudentClass::where('school_id', $data['school']->id)->count();
                $data['teacherCount']       =   Teacher::where('school_id', $data['school']->id)
                                                ->where("nip", "!=", "0")->count();
                return view("client.studentDashboard.showStudentDashboardPage", compact('data'));
        }
    
    // Process Edit
        public function processEdit (Request $request, string $code)
        {
            $data['school']     = AccountValidation::school($code);
            // Edit
                $newRecord              =   Student::find($data['school']['session']->id);
                $newRecord->name        =   $request->get('name');
                $newRecord->email       =   $request->get('email');
                $newRecord->place_birth =   $request->get('place_birth');
                $newRecord->date_birth  =   $request->get('date_birth');
                $newRecord->address     =   $request->get('address');
                $newRecord->phone       =   $request->get('phone');

                if ($request->hasFile("picture")) {
                    $studentImage       =   $request->file('picture');
                    $extstudentImage    =   $studentImage->getClientOriginalExtension();
                    $newName            =   rand(1111111,9999999) . '.' . $extstudentImage;
                    $studentImage->move('public/upload/school/' . $data['school']->code . '/', $newName);
                    $newRecord->picture =   'public/upload/school/' . $data['school']->code . '/' . $newName;
                    setcookie("53ad84c9696b4da263a0896291a8ed2f", 'public/upload/school/' . $data['school']->code . '/' . $newName, 0 , "/");
                }

                $newRecord->save();
            // Return
                return \Redirect::to(route("student/dashboard", $data['school']->code))->with("success", "Edit record success");
        }
}
