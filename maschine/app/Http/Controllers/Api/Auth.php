<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Client\AccountValidation;

use App\Model\Main\School;
use App\Model\Data\Teacher;
use App\Model\Data\Student;
use App\Model\Data\Token;

class Auth extends Controller
{
    // Login
        public function login (Request $request)    
        {
            //  Request Validation
                $requestValidation      =   \Validator::make($request->all(), [
                                                "school_code"   =>  "required",
                                                "role"          =>  "required",
                                                "username"      =>  "required",
                                                "password"      =>  "required"
                                            ]);
                if ($requestValidation->fails()) {
                    return response(["message"   =>  "Form is not complete"], 402);
                }
            // School Validation
                $data['school']     = School::where("code", $request->get("school_code"))->first();
                // Invalid School
                    if ($data['school'] == false) {
                        return response(["message"   =>  "Invalid account"], 401);
                    } 
                // Expired School
                    if (AccountValidation::expiredCheck($data['school'])) {
                        return response(["message"   =>  "Invalid account"], 401);
                    }
            // Role Branch
                /* 2 For Student & 1 For Teacher */
                    if ($request->get("role") == 1) {
                        // Checking Account
                            // By Username
                            $teacherRecord      =   Teacher::where("username", $request->get("username"))
                                                    ->where("school_id", $data['school']->id);
                            if ($teacherRecord->count() == 0) {
                                // Try By EMail
                                    $teacherRecord      =   Teacher::where("email", $request->get("username"))
                                                            ->where("school_id", $data['school']->id);
                                    if ($teacherRecord->count() == 0) {
                                        return response(["message"   =>  "Invalid account"], 401);
                                    }
                            }
                        // Compare Hash
                            $teacherRecord  =   $teacherRecord->first();
                            if (\Hash::check($request->get('password'), $teacherRecord->password)) {
                                // Generate New Token
                                    $token                  =   bcrypt($teacherRecord->name);
                                    $newRecord              =   new Token;
                                    $newRecord->school_id   =   $data['school']->id;
                                    $newRecord->role        =   1;
                                    $newRecord->account_id  =   $teacherRecord->id;
                                    $newRecord->token       =   $token;
                                    $newRecord->type        =   "Normal";
                                    $newRecord->save();
                                // Sending Feedback
                                    return response([
                                        "message"   =>  "Login success",
                                        "token"     =>  $token,
                                        "nickname"  =>  $teacherRecord->name
                                    ], 200);
                            } else {
                                return response(["message"   =>  "Invalid account"], 401);
                            }
                    } elseif ($request->get("role") == 2) {
                        // Check Student Record
                            $studentRecord      =   Student::where("nis", $request->get("username"))
                                                                ->where("school_id", $data['school']->id);
                            if ($studentRecord->count() == 0) {
                                return response(["message"   =>  "Invalid account"], 401);
                            }
                        // Check Hash
                            $studentRecord  =   $studentRecord->first();
                            if ($request->get('password') == $studentRecord->password) {
                                // Generate New Token
                                    if ($request->get("line_account")) {
                                        if ($request->get("line_source")) {
                                            $token              =   $request->get("line_source");
                                            $recordType         =   "Line";
                                        } else {
                                            return response(["message"   =>  "Invalid account"], 401);   
                                        }
                                    } else {
                                        $token              =   bcrypt($studentRecord->name);
                                        $recordType         =   "Normal";
                                    }
                                    $newRecord              =   new Token;
                                    $newRecord->school_id   =   $data['school']->id;
                                    $newRecord->role        =   2;
                                    $newRecord->account_id  =   $studentRecord->id;
                                    $newRecord->token       =   $token;
                                    $newRecord->type        =   $recordType;
                                    $newRecord->save();
                                // Sending Feedback
                                    return response([
                                        "message"   =>  "Login success",
                                        "token"     =>  $token,
                                        "nickname"  =>  $studentRecord->name
                                    ], 200);
                            } else {
                                return response(["message"   =>  "Invalid account"], 401);                            
                            }
                    } else {
                        return response(["message"   =>  "Invalid account"], 401);
                    }
            // If All Process Failed
                return response(["message"   =>  "Process failed"], 500);
        }   

    // Logout
        public function logout (Request $request)
        {   
            // Token Validation
                if (!$this->tokenValidation($request))  {
                    return response([
                        "message"   =>  "Invalid token"
                    ], 401);
                }
            // Check Token Is Exist
                $tokenRecord            =   Token::where("token", $request->header("Authorization"));
                $tokenRecord->delete();
            // Sending Feedback
                return response([
                    "message"   =>  "Logout success",
                ], 200);
        }

    // Token Validation
        public static function tokenValidation ($request, bool $student = true)
        {
            if ($request) {
                $tokenRecord            =   Token::where("token", $request->header("Authorization"));
                if ($tokenRecord->count() != 0) {
                    $tokenRecord        =   $tokenRecord->first();
                    $schoolRecord       =   School::find($tokenRecord->school_id);
                    if ($schoolRecord) {
                        if (!AccountValidation::expiredCheck($schoolRecord)) {
                            if ($student) {
                                return true;
                            } else {
                                $teacherRecord      =   Teacher::find($tokenRecord->account_id);
                                if ($teacherRecord) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
}
