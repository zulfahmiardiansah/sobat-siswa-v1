<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\Auth;

use App\Model\Main\School;
use App\Model\Data\Token;

class SchoolRes extends Controller
{
    // Get School Data
        public function getData (Request $request)
        {
            // Preparing Data
                $tokenRecord    =   Token::where("token", $request->header("Authorization"))->first();
                $responseData   =   School::find($tokenRecord->school_id);
            // Return
                return response($responseData);
        }
}
