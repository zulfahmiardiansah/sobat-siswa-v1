<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\Auth;

use App\Model\Data\StudentClass;
use App\Model\Data\Token;

class ClassRes extends Controller
{
    // Get Class Data
        public function getDatas (Request $request)
        {
            // Preparing Data
                $tokenRecord    =   Token::where("token", $request->header("Authorization"))->first();
                $responseData   =   StudentClass::where("school_id", $tokenRecord->school_id)
                                                ->orderBy("name", "ASC")
                                                ->get();
            // Return
                return response($responseData);
        }

    // Get Single Class Data
        public function getData (Request $request, int $id)
        {
            // Preparing Data
                $tokenRecord    =   Token::where("token", $request->header("Authorization"))->first();
                $responseData   =   StudentClass::where("school_id", $tokenRecord->school_id)
                                                ->where("id", $id)
                                                ->first();
            // Return
                return response($responseData);
        }
}
