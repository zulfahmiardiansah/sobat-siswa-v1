<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\School;

class Dashboard extends Controller
{
    // Show Dashboard Page
        public function showDashboardPage (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // Data
                $data['clientCount']        =   School::where("code", "!=", "demo")->count();
                foreach (\DB::select("SELECT SUM(nominal) as total FROM `main_log` WHERE YEAR(created_at) = YEAR(NOW())") as $a) {
                    $data['revenueCount']   =   $a->total;
                }
            // View
                return view('console.dashboard.showDashboardPage', compact('data'));
        }
}
