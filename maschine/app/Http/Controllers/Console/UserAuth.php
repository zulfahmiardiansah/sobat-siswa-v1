<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\User;

class UserAuth extends Controller
{
    // Show Login Page
        public function showLoginPage (Request $request)
        {
            if (isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/dashboard"));
            }
            return view('console.userAuth.showLoginPage');
        }
    
    // Process Login Page
        public function processLoginPage (Request $request)
        {
            // Validation
                $validationRule     =   \Validator::make($request->all(), [
                                                            "username" => "required",
                                                            "password" => "required"
                                                        ]);
                if ($validationRule->fails()) {
                    return \Redirect::to(route("console/login"))->with("error", "Username or password is wrong");
                }
            // Account
                $userRecord         =   User::where("username", $request->get('username'));
                if ($userRecord->count()){
                    $userRecord     =   $userRecord->first();
                    if (\Hash::check($request->get('password'), $userRecord->password)) {
                        setcookie("f8ee0d0b369ffa4e12c262a4cda32079", $userRecord->id, 0, "/");
                        setcookie("c72a6b673510b278aaeb5ee6f14aed4a", $userRecord->nickname, 0, "/");
                        setcookie("f7814c99a661d88f26f069ace43ad35e", $userRecord->role, 0, "/");
                        return \Redirect::to(route("console/dashboard"))->with("success", "Login success");
                    }
                }
            // Unknown Account 
                return \Redirect::to(route("console/login"))->with("error", "Unknown acount");
        }
    
    // Processs Logout
        public function processLogout (Request $request)
        {
            setcookie("f8ee0d0b369ffa4e12c262a4cda32079", false, -1, "/");
            setcookie("c72a6b673510b278aaeb5ee6f14aed4a", false, -1, "/");
            setcookie("f7814c99a661d88f26f069ace43ad35e", false, -1, "/");
            return \Redirect::to(route("console/login"))->with("success", "Logout success");
        }
}
