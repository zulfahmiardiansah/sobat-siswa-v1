<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\Feature;
use App\Model\Main\Plan;
use App\Model\Main\School;

class PlanPage extends Controller
{
    // Show Plan Page
        public function showPlanPage (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['plan']   =   Plan::orderBy('id', 'DESC')->paginate(15);
                return view('console.plan.showPlanPage', compact('data'));
        }

    // Show Edit Modal
        public function showEditModal (Request $request, int $id = 0)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['plan']    =   Plan::find($id);
                return view('console.plan.showEditModal', compact('data'));
        }

    // Process Edit
        public function processEdit (Request $request, int $id = 0)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // Edit
                $oldRecord              =   Plan::find($id);
                if ($oldRecord) {
                    $oldRecord->name        =   $request->get('name');
                    $oldRecord->price       =   $request->get('price');
                    $oldRecord->limit       =   $request->get('limit');
                    $oldRecord->limit_month =   $request->get('limit_month');
                    $oldRecord->description =   $request->get('description');
                    $oldRecord->save();
                }
            // Return
                return \Redirect::to(route("console/plan"))->with("success", "Edit record success");
        }
        
    // Show Add Modal
        public function showAddModal (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['feature']    =   Feature::orderBy("feature")->get();
                return view('console.plan.showAddModal', compact('data'));
        }
    
    // Process Add
        public function processAdd (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // Add
                $featureInput           =   $request->get('feature');
                if ($featureInput == null) {
                    return \Redirect::to(route("console/plan"))->with("error", "Please choose at least one feature");
                }
                foreach ($featureInput as $singleInput) {
                    $featureArray[]     =   [
                                                'code' => Feature::find($singleInput)->code,
                                                'feature' => Feature::find($singleInput)->feature,
                                            ];
                }
                $newRecord              =   new Plan;
                $newRecord->name        =   $request->get('name');
                $newRecord->price       =   $request->get('price');
                $newRecord->limit       =   $request->get('limit');
                $newRecord->limit_month =   $request->get('limit_month');
                $newRecord->description =   $request->get('description');
                $newRecord->feature     =   json_encode($featureArray);
                $newRecord->save();
            // Return
                return \Redirect::to(route("console/plan"))->with("success", "Add record success");
        }
    
    // Process Delete
        public function processDelete (Request $request, int $id)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // Delete
                $oldRecord          =   Plan::find($id);
                if ($oldRecord) {
                    if (School::where('plan_id', $oldRecord->id)->count()) {
                        return \Redirect::to(route("console/plan"))->with("error", "There are schools that use this plan");
                    }
                    $oldRecord->delete();
                }
            // Return
                return \Redirect::to(route("console/plan"))->with("success", "Delete record success");
        }
}
