<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\Feature;

class FeaturePage extends Controller
{
    // Show Feature Page
        public function showFeaturePage (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['feature']    =   Feature::orderBy('id', 'DESC')->paginate(10);
                return view('console.feature.showFeaturePage', compact('data'));
        }
    
    // Show Add Modal
        public function showAddModal (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                return view('console.feature.showAddModal');
        }
    
    // Process Add
        public function processAdd (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // Add
                $newRecord          =   new Feature;
                $newRecord->code    =   $request->get('code');
                $newRecord->feature =   $request->get('feature');
                $newRecord->save();
            // Return
                return \Redirect::to(route("console/feature"))->with("success", "Add record success");
        }

    // Show Edit Modal
        public function showEditModal (Request $request, int $id = 0)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['feature']    =   Feature::find($id);
                return view('console.feature.showEditModal', compact('data'));
        }

    // Process Edit
        public function processEdit (Request $request, int $id)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // Edit
                $oldRecord          =   Feature::find($id);
                if ($oldRecord) {
                    $oldRecord->code    =   $request->get('code');
                    $oldRecord->feature =   $request->get('feature');
                    $oldRecord->save();
                }
            // Return
                return \Redirect::to(route("console/feature"))->with("success", "Edit record success");
        }

    // Process Delete
        public function processDelete (Request $request, int $id)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // Delete
                $oldRecord          =   Feature::find($id);
                if ($oldRecord) {
                    $oldRecord->delete();
                }
            // Return
                return \Redirect::to(route("console/feature"))->with("success", "Delete record success");
        }

}
