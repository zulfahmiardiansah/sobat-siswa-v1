<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\Log;

class LogPage extends Controller
{
    // Show Log Page
        public function showLogPage (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['log']     =   Log::orderBy("id", "DESC")->paginate(15);
                return view('console.log.showLogPage', compact('data'));
        }
}
