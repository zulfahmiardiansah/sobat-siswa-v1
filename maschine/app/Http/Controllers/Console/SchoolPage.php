<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Main\Plan;
use App\Model\Main\Log;
use App\Model\Main\School;
use App\Model\Data\Teacher;

class SchoolPage extends Controller
{
    // Show School Page
        public function showSchoolPage (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['school']     =   School::select("main_school.*", "main_plan.name as plan_name", "main_plan.limit as plan_limit")
                                                ->join("main_plan", "main_school.plan_id", "main_plan.id")
                                                ->orderBy("main_school.id", "DESC")
                                                ->paginate(15);
                return view('console.school.showSchoolPage', compact('data'));
        }
    
    // Show Edit Info Modal
        public function showEditInfoModal (Request $request, int $id)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['school']    =   School::find($id);
                return view('console.school.showEditInfoModal', compact('data'));
        }

    // Show Add Modal
        public function showAddModal (Request $request)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['plan']    =   Plan::orderBy("id")->get();
                return view('console.school.showAddModal', compact('data'));
        }

    // Show Purchase Modal
        public function showPurchaseModal (Request $request, int $id)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['plan']    =   Plan::orderBy("id")->get();
                $data['school']    =   School::find($id);
                return view('console.school.showPurchaseModal', compact('data'));
        }

    // Process Add
        public function processAdd (Request $request)
        {
            $validation         =   \Validator::make($request->all(), [
                                                        "code"  =>  "required",
                                                        "name"  =>  "required",
                                                        "phone" =>  "required",
                                                        "email" =>  "required",
                                                        "address" => "required",
                                                        "plan_id" => "required",
                                                        "proof" => "required|image",
                                                        "logo" => "required|image"
                                                    ]);
            if ($validation->fails()) {
                return \Redirect::to(route("console/school"))->with("error", "Invalid form");
            }

            $planRecord         =   Plan::find($request->get('plan_id'));

            $proofImage         =   $request->file('proof');
            $extproofImage      =   $proofImage->getClientOriginalExtension();
            $newName            =   rand(1111111,9999999) . '.' . $extproofImage;
            $proofImage->move('public/upload/console/proof/', $newName);
            $logDescription     =   "Register :" . $request->get('name') . " With " . $planRecord->name . " For " . $planRecord->limit_month . " Month";

            $logRecord              =   new Log;
            $logRecord->description =   $logDescription;
            $logRecord->nominal     =   $planRecord->price;
            $logRecord->type        =   "+";
            $logRecord->proof       =   'public/upload/console/proof/' . $newName;
            $logRecord->save();

            $logoImage              =   $request->file('logo');
            $extlogoImage           =   $logoImage->getClientOriginalExtension();
            $newName                =   rand(1111111,9999999) . '.' . $extlogoImage;
            $logoImage->move('public/upload/school/logo/', $newName);

            $newRecord              =   new School;
            $newRecord->code        =   $request->get('code');
            $newRecord->name        =   $request->get('name');
            $newRecord->address     =   $request->get('address');
            $newRecord->email       =   $request->get('email');
            $newRecord->phone       =   $request->get('phone');
            $newRecord->lifetime    =   date("Y-m-d", strtotime(date("Y-m-d") . " +" . $planRecord->limit_month . " month"));
            $newRecord->logo        =   'public/upload/school/logo/' . $newName;
            $newRecord->plan_id     =   $planRecord->id;
            $newRecord->save();

            $newAccount             =   new Teacher;
            $newAccount->nip        =   00000000000;
            $newAccount->name       =   "Administrator";
            $newAccount->school_id  =   $newRecord->id;
            $newAccount->email      =   "admin@sobatsiswa.id";
            $newAccount->phone      =   "-";
            $newAccount->role       =   0;
            $newAccount->username   =   "admin";
            $newAccount->password   =   \Hash::make("ZoIaFa");
            $newAccount->save();

            return \Redirect::to(route("console/school"))->with("success", "Add record success");
        }

    // Process Edit
        public function processEdit (Request $request, int $id)
        {
            $validation         =   \Validator::make($request->all(), [
                                                        "code"  =>  "required",
                                                        "name"  =>  "required",
                                                        "phone" =>  "required",
                                                        "email" =>  "required",
                                                        "address" => "required",
                                                    ]);
            if ($validation->fails()) {
                return \Redirect::to(route("console/school"))->with("error", "Invalid form");
            }

            $newRecord              =   School::find($id);
            $newRecord->code        =   $request->get('code');
            $newRecord->name        =   $request->get('name');
            $newRecord->address     =   $request->get('address');
            $newRecord->email       =   $request->get('email');
            $newRecord->phone       =   $request->get('phone');
            $validation             =   \Validator::make($request->all(), [
                                            "logo" => "required|image"
                                        ]);
            if (!$validation->fails()) {
                $logoImage              =   $request->file('logo');
                $extlogoImage           =   $logoImage->getClientOriginalExtension();
                $newName                =   rand(1111111,9999999) . '.' . $extlogoImage;
                $logoImage->move('public/upload/school/logo/', $newName);
                $newRecord->logo        =   'public/upload/school/logo/' . $newName;
            }
            $newRecord->save();

            return \Redirect::to(route("console/school"))->with("success", "Edit record success");
        }

    // Process Purchase
        public function processPurchase (Request $request, int $id)
            {
                $validation         =   \Validator::make($request->all(), [
                                                            "plan_id" => "required",
                                                            "proof" => "required|image",
                                                        ]);
                if ($validation->fails()) {
                    return \Redirect::to(route("console/school"))->with("error", "Invalid form");
                }

                $planRecord         =   Plan::find($request->get('plan_id'));

                $proofImage         =   $request->file('proof');
                $extproofImage      =   $proofImage->getClientOriginalExtension();
                $newName            =   rand(1111111,9999999) . '.' . $extproofImage;
                $proofImage->move('public/upload/console/proof/', $newName);
                $logDescription     =   "Purchase :" . $request->get('name') . " With " . $planRecord->name . " For " . $planRecord->limit_month . " Month";

                $logRecord              =   new Log;
                $logRecord->description =   $logDescription;
                $logRecord->nominal     =   $planRecord->price;
                $logRecord->type        =   "+";
                $logRecord->proof       =   'public/upload/console/proof/' . $newName;
                $logRecord->save();

                $newRecord              =   School::find($id);
                $newRecord->lifetime    =   date("Y-m-d", strtotime(date("Y-m-d", strtotime($newRecord->lifetime)) . " +" . $planRecord->limit_month . " month"));
                $newRecord->plan_id     =   $planRecord->id;
                $newRecord->save();

                return \Redirect::to(route("console/school"))->with("success", "Purchase new plan success");
            }


    // Show Feature
        public function showFeature (Request $request, int $id = 0)
        {
            if (!isset($_COOKIE['f8ee0d0b369ffa4e12c262a4cda32079'])) {
                return \Redirect::to(route("console/login"));
            }
            // View
                $data['plan']    =   Plan::find($id);
                return view('console.school.showFeature', compact('data'));
        }
}
