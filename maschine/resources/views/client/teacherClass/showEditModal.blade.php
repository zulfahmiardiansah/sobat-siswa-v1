<form method="POST" action="./class/edit/{{ $data['class']->id }}">
    {{ csrf_field() }}
	<div class="modal-body bg-secondary">
		<div class="form-group">
            <label> Nama </label>
            <input autocomplete="off" type="text" class=" form-control-alternative form-control" name="name" value="{{ $data['class']->name }}" required="" />
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-xs btn-secondary"
			data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
	</div>
</form>