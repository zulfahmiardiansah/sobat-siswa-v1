@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-8 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-building text-primary"></i>&nbsp;&nbsp; Kelas</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">Dibuat saat</th>
                                <th scope="col" class="text-center" width="25%">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['class'] as $single)
                                <tr>
                                    <td>
                                        {{ $single->name }}
                                    </td>
                                    <td>
                                        {{ date("d M Y", strtotime($single->created_at)) }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('teacher/student', $data['school']->code) }}?class_id={{ $single->id }}&search=true">
                                            <button class="btn btn-warning btn-xs btn-icon-only mr-2" title="Lihat siswa">
                                                <i class="fa fa-user-graduate"></i>
                                            </button>
                                        </a>
                                        <button class="btn btn-success btn-xs btn-icon-only" onclick="showEdit({{ $single->id }})">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        <a href="./class/delete/{{ $single->id }}">
                                            <button class="btn btn-danger btn-xs btn-icon-only">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['class']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
                <div class="card-footer py-4 text-center"  style="overflow-x:auto">
                    {{ $data['class']->links() }}
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 mb-5">
            <div class="card bg-secondary shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Menambah Kelas Baru</h3>
                        </div>
                    </div>
                </div>
                <form action="./class/add" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group">    
                            <label class="form-control-label">
                                Nama
                            </label>
                            <input autocomplete="off" type="text" name="name" class="form-control form-control-alternative" placeholder="Masukkan Nama Kelas" required="">
                        </div>
                    </div>
                    <div class="card-footer py-4 text-right">
                        <button type="submit" class="btn btn-xs btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Data Kelas");
    $("#classPage").addClass("active");
    $(".school-data.featureNav").show(300);
    function showEdit(e) {
        $("#modalTitle").html("Mengubah Data Kelas");
        $("#modalContent").load("{{ route('teacher/class', $data['school']->code) .'/edit/' }}" + e);
        $("#modal").modal("show");
    }
</script>