<form method="POST" action="./admin/edit/{{ $data['teacher']->id }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal-body bg-secondary">
            <div class="form-group">
                <label> NIP </label>
                <input value="{{ $data['teacher']->nip }}" placeholder="Masukkan NIP" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="nip" required="" />
            </div>
            <div class="form-group">
                <label> Nama </label>
                <input value="{{ $data['teacher']->name }}" placeholder="Masukkan Nama" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="name" required="" />
            </div>
            <div class="form-group">
                <label> Jenis Kelamin </label>
                <select class=" form-control-alternative form-control" name="gender" required="">
                        <option value="{{ $data['teacher']->gender }}">
                        {{ $data['teacher']->gender }}
                        </option>
                        <option value="Unknown">
                        - Pilih Jenis Kelamin -
                        </option>
                        <option value="Laki-laki">
                        Laki-laki
                        </option>
                        <option value="Perempuan">
                        Perempuan
                        </option>
                </select>
            </div>
            <div class="form-group">
                <label> E-Mail </label>
                <input value="{{ $data['teacher']->email }}" placeholder="Masukkan E-Mail" autocomplete="off" type="email"
                        class=" form-control-alternative form-control" name="email" />
            </div>
            <div class="form-group">
                <label> No. Telp </label>
                <input value="{{ $data['teacher']->phone }}" placeholder="Masukkan No. Telp" autocomplete="off" type="phone"
                        class=" form-control-alternative form-control" name="phone" />
            </div>
            <div class="form-group">
                <label> Username </label>
                <input value="{{ $data['teacher']->username }}" placeholder="Masukkan Username" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="username" required="" />
            </div>
            <div class="form-group">
                <label> Role </label>
                <select id="role" class=" form-control-alternative form-control" name="role" required="">
                        <option value="{{ $data['teacher']->role }}">
                            <?php
                                switch ($data['teacher']->role) {
                                    case '0':
                                        echo "Super Admin";
                                        break;
                                    case '1':
                                        echo "Admin";
                                        break;
                                    case '2':
                                        echo "User";
                                        break;
                                }
                            ?>
                        </option>
                        <option value="2">
                        - Pilih Role -
                        </option>
                        <option value="0">
                            Super Administrator
                        </option>
                        <option value="1">
                            Administrator
                        </option>
                        <option value="2">
                            User
                        </option>
                </select>
            </div>
            <div class="form-group">
                <label> Ubah Password (Opsional)</label>
                <input placeholder="Ingin mengubah password ?" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="password"/>
            </div>
            <div class="form-group">
                    <label> Gambar (Opsional)</label>
                    <br>
                    <input autocomplete="off" type="file" name="picture" />
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-secondary"
            data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-xs btn-primary">Simpan</button>
    </div>
</form>
<script>
    $("#role").select2();
</script>