@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-12 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-chalkboard-teacher text-primary"></i>&nbsp;&nbsp; Guru</h3>
                        </div>
                        <div class="col-right">
                            <button class="btn btn-sm btn-primary" onclick="showAdd()">
                                Tambah
                            </button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>Gambar</th>
                                <th scope="col" width="15%">NIP</th>
                                <th scope="col" width="22%">Nama</th>
                                <th scope="col">Peran Akun</th>
                                <th scope="col">E-Mail</th>
                                <th scope="col" class="text-center" width="25%">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['teacher'] as $single)
                                <tr>
                                    <td>
                                        <div class="media align-items-center">
                                            <a href="" class="avatar rounded-circle mr-3" style="background-color: transparent;">
                                                <img alt="Image placeholder" src="{{ asset( ($single->picture) ? $single->picture : 'public/adminAssets/img/default.png'  ) }}">
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $single->nip }}
                                    </td>
                                    <td>
                                        {{ $single->name }}
                                    </td>
                                    <td>
                                        <?php
                                            switch ($single->role) {
                                                case '0':
                                                    echo "Super Admin";
                                                    break;
                                                case '1':
                                                    echo "Admin";
                                                    break;
                                                case '2':
                                                    echo "Teacher";
                                                    break;
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        {{ $single->email }}
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-success btn-xs btn-icon-only" onclick="showEdit({{ $single->id }})">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        <a href="./admin/delete/{{ $single->id }}">
                                            <button class="btn btn-danger btn-xs btn-icon-only">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['teacher']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
                <div class="card-footer py-4 text-center">
                    {{ $data['teacher']->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Data Guru");
    $("#teacherPage").addClass("active");
    $(".school-data.featureNav").show(300);
    function showAdd() {
        $("#modalTitle").html("Menambah Data Guru");
        $("#modalContent").load("{{ route('teacher/teacher', $data['school']->code) .'/add' }}");
        $("#modal").modal("show");
    }
    function showEdit(e) {
        $("#modalTitle").html("Mengubah Data Guru");
        $("#modalContent").load("{{ route('teacher/teacher', $data['school']->code) .'/edit/' }}" + e);
        $("#modal").modal("show");
    }
</script>