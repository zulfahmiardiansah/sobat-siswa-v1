<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $data['school']->name }}</title>
    <link rel="stylesheet" href="{{ asset('public/loginAssets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/loginAssets/css/all.css') }}">
    <link rel="manifest" href="{{ asset('public/manifest/manifest-' . $data['school']->code . '.json') }}">
    <link rel="icon" type="image/png" href="{{ asset('public/loginAssets/img/ros2.png') }}">
    <link rel="stylesheet" href="{{ asset('public/commonAssets/css/fakeLoader.css') }}">
    <style>
        .swal2-popup {
            width: 20em !important;
        }

        .swal2-icon {
            margin: 1em auto 1em !important;
        }

        .swal2-confirm.swal2-styled {
            transform: scale(0.9);
        }

        #particle {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: -9999;
        }

        .swal2-image {
            width: 200px;
            margin: 1em;
            max-width: 100%;
        }

        @media (min-width: 768px) {
            .fakeLoader {
                display: none;
            }
        }
    </style>
</head>

<body>
    <div class="fakeLoader"></div>
    <div id="particle"></div>
    <!-- <img src="{{ asset('public/loginAssets/img/path.svg') }}" alt="" class="path">
    <img src="{{ asset('public/loginAssets/img/path.svg') }}" alt="" class="path2"> -->
    <div id="wrapper">
        <div class="col-login">
            <div class="logo-container">
                <img src="{{ asset('public/loginAssets/img/ros.png') }}" class="companyLogo">
                <img src="{{ asset($data['school']->logo) }}" class="clientLogo">
            </div>
            <h1>
                Student Login
                <small>
                    Please login to your account
                </small>
            </h1>
            <hr>
            <form action="" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">
                        Student ID Number
                    </label>
                    <input name="nis" required="" autocomplete="off" type="text" class="form-control" autofocus=""
                        placeholder="Enter Your NIS">
                </div>
                <div class="form-group">
                    <label for="">
                        Password
                    </label>
                    <input name="password" required="" autocomplete="off" type="password" class="form-control"
                        placeholder="Enter Your Password">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn-primary">
                                <i class="fa fa-lock"></i> &nbsp;Login
                            </button>
                        </div>
                        <div class="col-6">
                            <a href="{{ url($data['school']->code) }}" class="btn-circle active">
                                <i class="fa fa-user-graduate"></i>
                            </a>
                            <a href="{{ url($data['school']->code) . '/teacher' }}" class="btn-circle">
                                <i class="fa fa-chalkboard-teacher"></i>
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    @if (Session::has('error'))
    <script>
        Swal.fire({
            title: 'Oops !',
            imageUrl: '{{ asset("public/adminAssets/img/mascot/failed.png") }}',
            confirmButtonColor: "#374897",
            text: '{{ Session::get('
            error ') }}',
        })

    </script>
    @endif
    <script src="{{ asset('public/adminAssets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/loginAssets/particleEffect/particles.min.js') }}"></script>
    <script src="{{ asset('public/commonAssets/js/fakeLoader.js') }}"></script>
    <script>
        particlesJS.load('particle', '{{ asset("public/loginAssets/particleEffect/particle.json") }}', function () {
            console.log('callback - particles.js config loaded');
        });

    </script>
    <script>
        $(document).ready(function(){
            $.fakeLoader({
                timeToHide:1200,
                bgColor:"#fff",
                spinner:"spinner7"
            });
        });
    </script>
</body>

</html>
