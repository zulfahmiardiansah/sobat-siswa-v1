@include('client/part/headerResource')

    <!-- Header -->
    <div class="header bg-gradient-primary pb-5 pt-5 pt-md-7">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <div class="row">
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">TANGGAL</h5>
                                        <span class="h2 font-weight-bold mb-0" id="time">{{ date("H.i.s") }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-clock"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-danger mr-2"><i class="fa fa-calendar"></i> &nbsp;{{ date("l") }}</span>
                                    <span class="text-nowrap">{{ date("d M Y") }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">JUMLAH SISWA</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $data['studentCount'] }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                                            <i class="fas fa-user-graduate"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-calendar"></i> &nbsp;{{ date("l") }}</span>
                                    <span class="text-nowrap">{{ date("d M Y") }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">JUMLAH GURU</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $data['teacherCount'] }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                            <i class="fas fa-chalkboard-teacher"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-yellow mr-2"><i class="fa fa-calendar"></i> &nbsp;{{ date("l") }}</span>
                                    <span class="text-nowrap">{{ date("d M Y") }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">JUMLAH KELAS</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ $data['classCount'] }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                            <i class="fas fa-building"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-muted text-sm">
                                    <span class="text-info mr-2"><i class="fa fa-calendar"></i> &nbsp;{{ date("l") }}</span>
                                    <span class="text-nowrap">{{ date("d M Y") }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--3">
        <div class="row mb-2">
            @foreach (json_decode($data['school']->feature) as $single)
                @include("client.teacherDashboard.widget." . $single->code . "TeacherWid")
            @endforeach
        </div>
    </div>
@include('client/part/footerResource')

<script>
    function startTime() {
        var asiaTime = new Date().toLocaleString("en-US", {
            timeZone: "Asia/Jakarta"
        });
        var today = new Date(asiaTime);
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML =
            h + "." + m + '.' + s;
        var t = setTimeout(startTime, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        };
        return i;
    }
    startTime();
    $("#pageTitle").html("Dasbor");
    $("#dashboardPage").addClass("active");
</script>