<div class="col-md-6 col-12 mb-4">
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title mb-0">
                Pelanggaran Terkini
            </h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-light">
                    <tr>
                        <th>
                            Waktu
                        </th>
                        <th>
                            Nama
                        </th>
                        <th>
                            Butir Peraturan
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['recentViolation'] as $single)
                    <tr>
                        <td>
                            <b>{{ date('H.i', strtotime($single->created_at)) }}</b><br>
                            {{ date('d M Y', strtotime($single->date)) }}
                        </td>
                        <td>
                            <a class="text-danger" href="attitude/violation/single/{{ $single->student_id }}">
                                <b>{{ explode(' ', $single->student_name)[0] }}</b> <br>
                                ({{ $single->class_name }})
                            </a>
                        </td>
                        <td>
                            {{ $single->description }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if (count($data['recentViolation']) == 0)
                <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                    <b>
                        Kosong
                    </b>
                </h3>
            @endif
        </div>
    </div>
</div>
<div class="col-md-6 col-12 mb-4">
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title mb-0">
                Prestasi Terkini
            </h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-light">
                    <tr>
                        <th>
                            Nama
                        </th>
                        <th width="60%">
                            Deskripsi
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['recentAchievement'] as $single)
                    <tr>
                        <td>
                            <a class="text-primary" href="attitude/achievement/single/{{ $single->student_id }}">
                                <b>{{ explode(' ', $single->student_name)[0] }}</b> <br>
                                ({{ $single->class_name }})
                            </a>
                        </td>
                        <td>
                            {{ $single->name }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if (count($data['recentAchievement']) == 0)
                <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                    <b>
                        Kosong
                    </b>
                </h3>
            @endif
        </div>
    </div>
</div>
<style>
    table tr td, table tr th {
        vertical-align: middle;
    }
</style>