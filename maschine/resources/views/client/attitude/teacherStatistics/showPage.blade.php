@include('client/part/headerResource')

<style>
    .row .form-control {
        padding-right: 50px;
    }

    .row .form-group {
        max-width: 100%;
        position: relative;
        width: 500px;
        margin: auto;
    }

    .row button.searchButton {
        position: absolute;
        right: 0;
        top: 0;
        margin: 9px;
    }
</style>

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--8">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 mb-4">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 mb-4">
            @if (isset($_GET['date']))
                <h3 class="mt-3 text-center text-white mb-3">
                    Berikut ini <b>hasil pencarian</b> untuk {{ date("F Y", strtotime( str_replace("/", "-", "01/".$_GET['date']) )) }}
                </h3>
            @endif
            <form action="" mehod="GET" class="mb-1">
                <div class="form-group">
                @if (isset($_GET['date']))
                <input value="{{ $_GET['date'] }}" name="date" type="text" class="form-control form-control-alternative from" placeholder="Ingin mencari pada waktu tertentu ?">
                @else
                <input name="date" type="text" class="form-control form-control-alternative from" placeholder="Ingin mencari pada waktu tertentu ?">
                @endif
                    <button class="btn btn-sm btn-primary searchButton">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 mb-3">
            <div class="card shadow mb-4">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-gavel text-primary"></i>&nbsp;&nbsp; Trend Pelanggaran Pada Bulan {{ $data['monthTitle'] }}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>
                                    No.
                                </th>
                                <th>
                                    Case
                                </th>
                                <th>
                                    Rule
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $h = 1; ?>
                            @foreach ($data['violationRank'] as $single)
                            <tr>
                                <td>
                                    <span class="badge badge-danger">
                                        <b>
                                            {{ $h++ }}
                                        </b>
                                    </span>
                                </td>
                                <td>
                                    <b>{{ $single->total }} Case</b>
                                </td>
                                <td>
                                    {{ $single->code }}. {{ $single->description }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['violationRank']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-chart-bar text-primary"></i>&nbsp;&nbsp; Kasus Pelanggaran Hingga {{ $data['monthTitle'] }}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="overflow-x: auto"
                    style="padding: 20px; box-sizing: box-sizing: border-box;">
                    <!-- Chart -->
                    <!-- Chart wrapper -->
                    <canvas id="violationChart" class="chart-canvas chartjs-render-monitor"
                        style="display: block; "></canvas>

                </div>
            </div>
            <div class="card shadow">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-building text-primary"></i>&nbsp;&nbsp; Kelas Pelanggaran Terbanyak {{ $data['monthTitle'] }}</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="overflow-x: auto"
                    style="padding: 20px; box-sizing: box-sizing: border-box;">
                    <canvas id="classChart" class="chart-canvas chartjs-render-monitor"
                        style="display: block; height: auto !important;" height="180"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow mb-4">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-chart-bar text-primary"></i>&nbsp;&nbsp; Prestasi Pada Bulan {{ $data['monthTitle'] }}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="overflow-x: auto"
                    style="padding: 20px; box-sizing: box-sizing: border-box;">
                    <!-- Chart -->
                    <!-- Chart wrapper -->
                    <canvas id="achievementRegionChart" class="chart-canvas chartjs-render-monitor"
                        style="display: block;" height="200"></canvas>

                </div>
            </div>
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-trophy text-primary"></i>&nbsp;&nbsp; Jenis Prestasi Pada Bulan {{ $data['monthTitle'] }}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="overflow-x: auto"
                    style="padding: 20px; box-sizing: box-sizing: border-box;">
                    <!-- Chart -->
                    <!-- Chart wrapper -->
                    <canvas id="achievementTypeChart" class="chart-canvas chartjs-render-monitor"
                        style="display: block;" height="200"></canvas>

                </div>
            </div>
        </div>
    </div>
    <br>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Attitude");
    $("#statisticPage").addClass("active");
    $(".attitude.featureNav").show(300);
    $('.from').datepicker({
        autoclose: true,
        minViewMode: 1,
        format: 'mm/yyyy'
    })
    var chartA = new Chart($("#violationChart"), {
			type: 'line',
			options: {
                legend: {
                    display: false
                },
				tooltips: {
					callbacks: {
						label: function(item, data) {
							var label = data.datasets[item.datasetIndex].label || '';
							var yLabel = item.yLabel;
							var content = '';
							content += yLabel + ' Case';
							return content;
						}
					}
				}
			},
			data: {
				labels: [
                    <?php
                        foreach($data['violationGraph'] as $single):
                            echo '"'. date("M Y", strtotime($single[0]->date_test)) . '",';
                        endforeach;
                    ?>
                ],
				datasets: [{
					label: 'Data',
					data: [
                        <?php
                            foreach($data['violationGraph'] as $single):
                                echo $single[0]->total . ',';
                            endforeach;
                        ?>
                    ],
                    backgroundColor: "rgba(240, 147, 43, .6)",
                    borderColor: "rgba(240, 147, 43, .6)"
				}]
			}
        });
    var chartB = new Chart($("#classChart"), {
        type: 'bar',
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(item, data) {
                        var label = data.datasets[item.datasetIndex].label || '';
                        var yLabel = item.yLabel;
                        var content = '';
                        content += yLabel + ' Case';
                        return content;
                    }
                }
            }
        },
        data: {
            labels: [
                <?php
                    foreach($data['violationClassTable'] as $single):
                        echo "'" . $single->name . "',";
                    endforeach;
                ?>
            ],
            datasets: [{
                label: 'Data',
                data: [
                    <?php
                        foreach($data['violationClassTable'] as $single):
                            echo $single->total . ",";
                        endforeach;
                    ?>
                ],
                backgroundColor: "rgba(251, 99, 64, .6)",
                borderColor: "rgba(251, 99, 64, .6)"
            }]
        }
    });
    var chartC = new Chart($("#achievementRegionChart"), {
        type: 'bar',
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(item, data) {
                        var label = data.datasets[item.datasetIndex].label || '';
                        var yLabel = item.yLabel;
                        var content = '';
                        content += yLabel + ' Case';
                        return content;
                    }
                }
            }
        },
        data: {
            labels: [
                <?php
                    $achievementRegionCategory          =   ['Wilayah', 'Kota', 'Provinsi', 'Nasional', 'Internasional', 'Tidak Berkategori'];
                    $number                             =   0;
                    foreach($data['achievementRegion'] as $single):
                        if ($single) {
                            foreach ($single as $item):
                                echo "'" . $item->area . "',";
                            endforeach;
                        } else {
                            echo "'" . $achievementRegionCategory[$number] . "',";
                        }
                        $number++;
                    endforeach;
                ?>
            ],
            datasets: [{
                label: 'Data',
                data: [
                    <?php
                        foreach($data['achievementRegion'] as $single):
                            if ($single) {
                                foreach ($single as $item):
                                    echo $item->total. ',';
                                endforeach;
                            } else {
                                echo 0 . ',';
                            }
                        endforeach;
                    ?>
                ],
                backgroundColor: "rgba(45, 206, 137, .6)",
                borderColor: "rgba(45, 206, 137, .6)"
            }]
        }
    });
    var chartD = new Chart($("#achievementTypeChart"), {
        type: 'pie',
        options: {
            legend: {
                display: true
            }
        },
        data: {
            labels: [
                <?php
                    $achievementTypeCategory          =   ['Akademis', 'Non-Akademis', 'Tidak Berkategori'];
                    $number                             =   0;
                    foreach($data['achievementType'] as $single):
                        if ($single) {
                            foreach ($single as $item):
                                echo "'" . $item->type . "',";
                            endforeach;
                        } else {
                            echo "'" . $achievementTypeCategory[$number] . "',";
                        }
                        $number++;
                    endforeach;
                ?>
            ],
            datasets: [{
                label: 'Data',
                data: [
                    <?php
                        foreach($data['achievementType'] as $single):
                            if ($single) {
                                foreach ($single as $item):
                                    echo $item->total. ',';
                                endforeach;
                            } else {
                                echo 0 . ',';
                            }
                        endforeach;
                    ?>
                ],
                backgroundColor: [
                    "rgba(104, 109, 224,0.6)",
                    "rgba(224, 86, 253,0.6)",
                    "rgba(34, 166, 179,0.6)"
                ],
                borderColor: [
                    "rgba(104, 109, 224,0.6)",
                    "rgba(224, 86, 253,0.6)",
                    "rgba(34, 166, 179,0.6)"
                ]
            }]
        }
    });
</script>
