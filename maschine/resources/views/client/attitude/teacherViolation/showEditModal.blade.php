<form method="POST" action="./{{ $data['studentId'] }}/edit/{{ $data['violation']->id }}">
    {{ csrf_field() }}
	<div class="modal-body bg-secondary">
		<div class="form-group" id="ruleContainer">
            <label> Peraturan </label>
            <select id="rule" autocomplete="off" type="text" class=" form-control-alternative form-control" name="rule_id" required="">
				<option value="{{ $data['ruleSingle']->id }}">
					{{ $data['ruleSingle']->code }}. {{ $data['ruleSingle']->description }}
				</option>
				<option value="0">
					- Pilih Peraturan -
				</option>	
				@foreach ($data['rule'] as $single)
					<option value="{{ $single->id }}">
						{{ $single->code }}. {{ $single->description }}
					</option>
				@endforeach	
			</select>
		</div>
		<div class="form-group">
            <label> Tanggal </label>
            <input value="{{ date('Y-m-d', strtotime($data['violation']->date)) }}" placeholder="Masukkan tanggal" autocomplete="off" type="date" class=" form-control-alternative form-control" name="date" required="" />
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-xs btn-secondary"
			data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
	</div>
</form>
<script>
    $('#rule').select2({
        dropdownParent: $('#ruleContainer')
    });
</script>