<table>
    <tr>
        <td width="13">
            <b>NIS</b>
        </td>
        <td width="50">: {{ $data['student']->nis }}</td>
    </tr>
    <tr>
        <td>
            <b>Nama</b>
        </td>
        <td>: {{ $data['student']->name }}</td>
    </tr>
    <tr>
        <td>
            <b>Kelas</b>
        </td>
        <td>: {{ $data['student']->class_name }}</td>
    </tr>
    <tr>
        <td>
            <b>Jenis Kelamin</b>
        </td>
        <td>: {{ $data['student']->gender }}</td>
    </tr>
    <tr>
        <td>
            <b>Sanksi</b>
        </td>
        <td>
            : {{ $data['violationCount'] }} Poin
        </td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <b>
                Tanggal
            </b>
        </td>
        <td>
            <b>
                Data
            </b>
        </td>
        <td> 
            <b>
                Jumlah Poin
            </b>
        </td>
    </tr>
    @foreach ($data['violation'] as $single)
    <tr>
        <td>
            {{ date("d M Y", strtotime($single->date)) }}
        </td>
        <td>
            {{ $single->code }}. {{ $single->description }}
        </td>
        <td width="20">
            {{ $single->sanction }} Poin
        </td>
    </tr>
    @endforeach
    <tr>
        <td colspan="2">
        <b>Total</b>
        </td>
        <td>
        {{ $data['violationCount'] }}
        </td>
    </tr>
</table>
