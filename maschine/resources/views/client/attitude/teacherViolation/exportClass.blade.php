<style>
    table tbody tr td, table tr tbody th {
        border: 1px solid black;
    }
</style>
<table>
    <thead>
        <tr>
            <th width="15">
                <b>
                    NIS
                </b>
            </th>
            <th width="30">
                <b>
                    Nama
                </b>
            </th>
            <th width="13">
                <b>
                    Jenis Kelamin
                </b>
            </th>
            <th width="18">
                <b>
                    Jumlah Poin
                </b>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data['student'] as $single)
            <tr>
                <td>
                    {{ $single->nis }}
                </td>
                <td>
                    {{ $single->name }}
                </td>
                <td>
                    {{ $single->gender }}
                </td>
                <td>
                    @if (isset($data['violation']["$single->nis"]))
                        {{ $data['violation']["$single->nis"] }} Poin
                    @else
                        0 Poin
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>