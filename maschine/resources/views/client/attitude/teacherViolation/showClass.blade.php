@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-12 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-exclamation-circle text-primary"></i>&nbsp;&nbsp; Rekapitulasi Pelanggaran</h3>
                        </div>
                        <div class="col-right">
                            @if ($data['school']['session']->role < 2)
                                <a href="{{ $data['class']->id }}/export">
                                <button class="btn btn-sm btn-success">
                                    Export
                                </button>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <b>Kelas</b>
                                </td>
                                <td>:</td>
                                <td>{{ $data['class']->name }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        Jumlah Siswa
                                    </b>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    {{ count($data['student']) }} Student
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body bg-secondary" style="border-top: 1px solid #e9ecef">
                    <input type="text" class="form-control form-control-alternative" id="filterInput" onkeyup="filterTable()" placeholder="Ingin mencari lebih lanjut ?">
                </div>
                <div class="table-responsive">
                    <table class="table table-striped align-items-center table-flush docTable" id="filterTable">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="20%">NIS</th>
                                <th scope="col">Nama</th>
                                <th scope="col" width="15%">Jenis Kelamin</th>
                                <th scope="col" width="25%">Jumlah Poin</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['student'] as $single)
                                <tr>
                                    <td>
                                        {{ $single->nis }}
                                    </td>
                                    <td class="filterClass">
                                        {{ $single->name }}
                                    </td>
                                    <td>
                                        {{ $single->gender }}
                                    </td>
                                    <td>
                                        @if (isset($data['violation']["$single->nis"]))
                                            {{ $data['violation']["$single->nis"] }} Poin
                                        @else
                                            0 Poin
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['student']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')
<script>
    $("#pageTitle").html("Pelanggaran");
    $(".attitude.featureNav").show(300);
    $("#violationPage").addClass("active");
</script>