@if (count($data['student']))
    <hr class="m-0" style="border-top: 1px solid #eee;">
    <div class="text-center bg-secondary pb-3 pt-3 bt-0">
        <a href="./violation/class/{{ $data['id'] }}">
        <button class="btn btn-primary btn-sm">
            Lihat Rekapitulasi
        </button>
        </a>
    </div>
@endif
<div class="p-3" style="border-top: 1px solid #e9ecef;">
    <input type="text" class="form-control form-control-alternative" id="filterInput" onkeyup="filterTable()" placeholder="Ingin mencari lebih lanjut ?">
</div>
<table class="table table-striped" id="filterTable">
    <thead class="thead-light">
        <tr>
            <th width="20%">
                NIS
            </th>
            <th>
                Nama
            </th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data['student'] as $single)
            <tr>
                <td>
                    <a href="{{ route('teacher/attitude/violation', $data['school']->code) }}/single/{{ $single->id }}">
                        {{ $single->nis }}
                    </a>
                </td>
                <td class="filterClass">
                    <a href="{{ route('teacher/attitude/violation', $data['school']->code) }}/single/{{ $single->id }}">
                        {{ $single->name }}
                    </a>
                </td>
                <td>
                    <a href="{{ route('teacher/attitude/violation', $data['school']->code) }}/single/{{ $single->id }}">
                        <span class="badge badge-success">
                            <i class="fa fa-eye"></i>
                        </span>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@if (count($data['student']) == 0)
    <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
    <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
        <b>
            Kosong
        </b>
    </h3>
@endif