<form method="POST" action="./{{ $data['studentId'] }}/add"  enctype="multipart/form-data">
    {{ csrf_field() }}
	<div class="modal-body bg-secondary">
		<div class="form-group">
            <label> Nama * </label>
            <input placeholder="Masukkan Nama" autocomplete="off" type="text" class=" form-control-alternative form-control" name="name" required="" />
		</div>
		<div class="form-group">
            <label> Penyelenggara * </label>
            <input placeholder="Masukkan Penyelenggara" autocomplete="off" type="text" class=" form-control-alternative form-control" name="organizer" required="" />
		</div>
		<div class="form-group">
            <label> Deskripsi * </label>
            <textarea spellcheck="false" placeholder="Masukkan Deskripsi" autocomplete="off" class=" form-control-alternative form-control" name="description" required=""></textarea>
		</div>
		<div class="form-group">
            <label> Tanggal * </label>
            <input placeholder="Masukkan Tanggal" autocomplete="off" type="date" class=" form-control-alternative form-control" name="date" required="" />
		</div>
		<div class="form-group">
			<label> Wilayah * </label>
			<select name="area" class="form-control-alternative form-control" required="">
				<option value="Tidak Berkategori">- Pilih Wilayah -</option>
				<option value="Wilayah">Wilayah</option>
				<option value="Kota">Kota</option>
				<option value="Provinsi">Provinsi</option>
				<option value="Nasional">Nasional</option>
				<option value="Internasional">Internasional</option>
			</select>
		</div>
		<div class="form-group">
			<label> Jenis * </label>
			<select name="type" class="form-control-alternative form-control" required="">
				<option value="Tidak Berkategori">- Pilih Jenis -</option>
				<option value="Akademis">Akademis</option>
				<option value="Non-Akademis">Non-Akademis</option>
			</select>
		</div>
		<div class="form-group">
            <label> Sertifikat </label>
            <input class="d-block" type="file" name="certificate" />
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-xs btn-secondary"
			data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
	</div>
</form>