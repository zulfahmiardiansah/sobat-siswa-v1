@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-5 col-sm-5 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-user text-primary"></i>&nbsp;&nbsp; Biodata</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    <b>NIS</b>
                                </td>
                                <td>:</td>
                                <td>{{ $data['student']->nis }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Nama</b>
                                </td>
                                <td>:</td>
                                <td>{{ $data['student']->name }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Kelas</b>
                                </td>
                                <td>:</td>
                                <td>{{ $data['student']->class_name }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Jenis Kelamin</b>
                                </td>
                                <td>:</td>
                                <td>{{ $data['student']->gender }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-trophy text-primary"></i>&nbsp;&nbsp; Prestasi</h3>
                        </div>
                        <div class="col-right">
                            @if ($data['school']['session']->role < 2)
                                <button class="btn btn-sm btn-primary" onclick="showAdd()">
                                    Tambah
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="30%">Nama</th>
                                <th scope="col">Deskripsi</th>
                                @if ($data['school']['session']->role < 1) 
                                    <th scope="col" class="text-center" width="25%">Opsi</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['achievement'] as $single)
                            <tr>
                                <td style="white-space: normal">
                                    {{ $single->name }}
                                    <br><b>{{ date('d F Y', strtotime($single->date)) }}</b>
                                    ({{ $single->area != false ? $single->area : "Uncategorized" }} - {{ $single->type != false ? $single->type : "Uncategorized" }} )
                                </td>
                                <td style="white-space: normal">
                                    <div>
                                        {{ $single->description }} - Diadakan oleh {{ $single->organizer }}
                                    </div>
                                    @if ($single->certificate != '' && $single->certificate != null)
                                        <a class="badge badge-md badge-warning mt-2" download="" href="{{ asset($single->certificate) }}" title="Lihat Sertifikat">
                                            <i class="fa fa-download"></i>&nbsp; Unduh Sertifikat
                                        </a>
                                    @endif
                                </td>
                                @if ($data['school']['session']->role < 1) 
                                <td class="text-center">
                                        <button class="btn btn-success btn-sm btn-icon-only"
                                            onclick="showEdit({{ $single->id }})" title="Ubah">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        <a href="./{{ $data['student']->id }}/delete/{{ $single->id }}" title="Hapus">
                                            <button class="btn btn-danger btn-sm btn-icon-only">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['achievement']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
                <div class="card-footer py-4 text-center">
                    {{ $data['achievement']->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Prestasi");
    $(".attitude.featureNav").show(300);
    $("#achievementPage").addClass("active");
    function showAdd() {
        $("#modalTitle").html("Menambah Data Prestasi");
        $("#modalContent").load("{{ route('teacher/attitude/achievement', $data['school']->code) .'/single/'. $data['student']->id .'/add' }}");
        $("#modal").modal("show");
    }
    function showEdit(e) {
        $("#modalTitle").html("Mengubah Data Prestasi");
        $("#modalContent").load("{{ route('teacher/attitude/achievement', $data['school']->code) .'/single/'. $data['student']->id .'/edit/' }}" + e);
        $("#modal").modal("show");
    }
</script>