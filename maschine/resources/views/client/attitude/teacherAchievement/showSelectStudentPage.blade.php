@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-5 col-sm-5 col-xs-12 mb-5">
            <div class="card shadow bg-secondary">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-trophy text-primary"></i>&nbsp;&nbsp; Prestasi</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-control-label">
                            Kelas
                        </label>
                        <select onchange="getStudent(this)" id="class_id" name="class_id" class="form-control form-control-alternative">
                            <option value="0">
                                - Pilih Kelas -
                            </option>
                            @foreach ($data['class'] as $single)
                                <option value="{{ $single->id }}">
                                    {{ $single->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-users text-primary"></i>&nbsp;&nbsp; Data Siswa</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="getStudent">
                    <hr class="m-0" style="border-top: 1px solid #eee;">
                    <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                    <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                        <b>
                            Pilih Kelas Terlebih Dahulu
                        </b>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Prestasi");
    $("#achievementPage").addClass("active");
    $(".attitude.featureNav").show(300);
    $("#class_id").select2();
    function getStudent (e) {
        var a   =   $(e).val();
        $("#getStudent").load("{{ route('teacher/attitude/achievement', $data['school']->code) }}/getStudent/" + a)
    }
</script>