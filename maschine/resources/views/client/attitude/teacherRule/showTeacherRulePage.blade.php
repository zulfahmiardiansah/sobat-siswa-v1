@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-12 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-gavel text-primary"></i>&nbsp;&nbsp; Peraturan</h3>
                        </div>
                        <div class="col-right">
                            @if ($data['school']['session']->role < 1)
                                <button class="btn btn-sm btn-primary" onclick="showAdd()">
                                    Tambah
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table-striped table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="15%">Kode</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col" width="10%">Sanksi</th>
                                @if ($data['school']['session']->role < 1)
                                    <th scope="col" class="text-center" width="25%">Opsi</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['rule'] as $single)
                                <tr>
                                    <td>
                                        {{ $single->code }}
                                    </td>
                                    <td>
                                        {{ $single->description }}
                                    </td>
                                    <td>
                                        {{ $single->sanction }} Poin
                                    </td>
                                    @if ($data['school']['session']->role < 1)
                                            <td class="text-center">
                                                <button class="btn btn-success btn-xs btn-icon-only" onclick="showEdit({{ $single->id }})">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <a href="./rule/delete/{{ $single->id }}">
                                                    <button class="btn btn-danger btn-xs btn-icon-only">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </a>
                                            </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4 text-center">
                    {{ $data['rule']->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Peraturan");
    $(".attitude.featureNav").show(300);
    $("#rulePage").addClass("active");
    function showAdd() {
        $("#modalTitle").html("Menambah Butir Peraturan");
        $("#modalContent").load("{{ route('teacher/attitude/rule', $data['school']->code) .'/add' }}");
        $("#modal").modal("show");
    }
    function showEdit(e) {
        $("#modalTitle").html("Mengubah Butir Peraturan");
        $("#modalContent").load("{{ route('teacher/attitude/rule', $data['school']->code) .'/edit/' }}" + e);
        $("#modal").modal("show");
    }
</script>