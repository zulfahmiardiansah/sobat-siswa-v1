<form method="POST" action="./rule/add">
    {{ csrf_field() }}
	<div class="modal-body bg-secondary">
		<div class="form-group">
            <label> Kode </label>
            <input placeholder="Masukkan Kode" autocomplete="off" type="text" class=" form-control-alternative form-control" name="code" required="" />
		</div>
		<div class="form-group">
            <label> Deskripsi </label>
            <textarea spellcheck="false" placeholder="Masukkan Deskripsi" autocomplete="off" class=" form-control-alternative form-control" name="description" required=""></textarea>
		</div>
		<div class="form-group">
            <label> Sanksi / Point </label>
            <input placeholder="Masukkan Sanksi" autocomplete="off" type="number" min="1" class=" form-control-alternative form-control" name="sanction" required="" />
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-xs btn-secondary"
			data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
	</div>
</form>