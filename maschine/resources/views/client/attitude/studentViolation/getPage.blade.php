@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-5 col-sm-5 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-user text-primary"></i>&nbsp;&nbsp; Biodata</h3>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td colspan="3"><b>{{ $data['student']->name }}</b></td>
                            </tr>
                            <tr>
                                <td>
                                    <b>NIS</b>
                                </td>
                                <td>:</td>
                                <td>{{ $data['student']->nis }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Kelas</b>
                                </td>
                                <td>:</td>
                                <td>{{ $data['student']->class_name }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Jumlah Poin</b>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    {{ $data['violationCount'] }} Poin
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-exclamation-circle text-primary"></i>&nbsp;&nbsp; Pelanggaran</h3>
                        </div>
                        <div class="col-right">
                            <a href="violation/export">
                                <button class="btn btn-sm btn-success">
                                    Export
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="60%">Deskripsi</th>
                                <th scope="col" width="15%">Sanksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['violation'] as $single)
                            <tr>
                                <td style="white-space: normal">
                                    {{ $single->code }}. {{ $single->description }} &nbsp; ({{ date("d M Y", strtotime($single->date)) }})
                                </td>
                                <td>
                                    {{ $single->sanction }} Poin
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['violation']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
                <div class="card-footer py-4 text-center">
                    {{ $data['violation']->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Data Pelanggaran");
    $(".attitude.featureNav").show(300);
    $("#violationPage").addClass("active");
</script>