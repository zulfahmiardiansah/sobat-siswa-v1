<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="bgWhite" id="particle"></div>
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
            aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand pt-0">
            <img src="{{ asset($data['school']->logo) }}" class="navbar-brand-img d-none d-md-inline-block appLogo">
            <h3 class="d-sm-block d-md-none" id="mobileTitle" style="opacity: 0; font-weight: bold; transition: .5s;"></h3>
        </a>        
            <a class="h4 mb-0 text-uppercase d-none d-md-inline-block text-center" style="font-weight: bold;"
                href="">{{ $data['school']->name }}</a>
            <a class="h5 mb-0 text-uppercase d-none d-md-inline-block text-center" href="">Oleh Sobat Siswa</a>
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            @if (isset($data['school']['session']->profileImg))
                                <img alt="Image placeholder"
                                    src="{{ asset($data['school']['session']->profileImg) }}">
                            @else
                                <img alt="Image placeholder"
                                    src="{{ asset('public/adminAssets/img/theme/team-4-800x800.jpg') }}">
                            @endif
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Hai, {{ $data['school']['session']->name }} !</h6>
                    </div>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout', $data['school']->code) }}" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Keluar</span>
                    </a>
                </div>
            </li>
        </ul>
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-10 collapse-brand">
                        <a href="">
                            <a class="h4 mb-0 text-uppercase d-md-none d-lg-block text-center"
                                style="font-weight: bold;" href="">{{ $data['school']->name }}</a>
                            <br><a class="h5 mb-0 text-uppercase d-md-none d-lg-block text-center" href="">Oleh Sobat
                                Siswa</a>
                        </a>
                    </div>
                    <div class="col-2 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                            data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                            aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="appNav" style="margin-bottom: 10px;">
                @include("client.part.navigation")
            </div>
        </div>
    </div>
</nav>

<div class="main-content">
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block"><b><span class="text-capitalize" id="pageTitle"></span></b></a>
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                @if (isset($data['school']['session']->profileImg))
                                    <img alt="Image placeholder"
                                        src="{{ asset($data['school']['session']->profileImg) }}">
                                @else
                                    <img alt="Image placeholder"
                                        src="{{ asset('public/adminAssets/img/theme/team-4-800x800.jpg') }}">
                                @endif
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">{{ $data['school']['session']->name }}</span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Hai, {{ $data['school']['session']->name }} !</h6>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout', $data['school']->code) }}" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Keluar</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
