<footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              © 2019 <a href="https://www.sobatsiswa.id" class="font-weight-bold ml-1" target="_blank">Revolution Of School</a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="https://sobatsiswa.id/category/blog/" class="nav-link" target="_blank"><i class="fab fa-wordpress"></i>&nbsp; Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://www.instagram.com/revolutionofschool/" class="nav-link" target="_blank"><i class="fab fa-instagram"></i>&nbsp; Instagram</a>
              </li>
              <li class="nav-item">
                <a href="https://www.youtube.com/channel/UCYlwTLueDPsSAhAa6blRXCA" class="nav-link" target="_blank"><i class="fab fa-youtube"></i>&nbsp; Youtube</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
        </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modalTitle"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="modalContent">

                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('public/adminAssets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('public/adminAssets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://unpkg.com/simplebar@latest/dist/simplebar.min.js"></script>
    <script src="{{ asset('public/adminAssets/js/argon.js') }}"></script>
    <script src="{{ asset('public/adminAssets/vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('public/adminAssets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <script src="{{ asset('public/loginAssets/particleEffect/particles.min.js') }}"></script>
    
    @if (Session::has('error'))
    <script>
        Swal.fire({
            title: 'Oops !',
            imageUrl: '{{ asset("public/adminAssets/img/mascot/failed.png") }}',
            confirmButtonColor: "#374897",
            text: '{{ Session::get('error') }}',
        })
    </script>
    @endif
    @if (Session::has('success'))
    <script>
        Swal.fire({
            title: 'Yeay !',
            imageUrl: '{{ asset("public/adminAssets/img/mascot/ok.png") }}',
            confirmButtonColor: "#374897",
            text: '{{ Session::get('success') }}',
        })
    </script>
    @endif

    <script>

        function openMenu (a) {
            $(a).toggle(300);
        }
        $(".featureNav").hide();
        $(document).ready(function () {
            $("#mobileTitle").html($("#pageTitle").html());
            $("#mobileTitle").css('opacity', 1);
        });
        
        function filterTable () {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("filterTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByClassName("filterClass")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
        
        particlesJS.load('particle', '{{ asset("public/loginAssets/particleEffect/particle.json") }}', function() {
            console.log('callback - particles.js config loaded');
        });
        

    </script>
</body>
</html>