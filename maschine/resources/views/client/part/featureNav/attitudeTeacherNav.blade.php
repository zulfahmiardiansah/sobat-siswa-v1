<hr class="my-3">
<h6 class="navbar-heading text-muted menuTrigger" onclick="openMenu('.attitude')">
    <div class="row">
        <div class="col-9">
            Evaluasi Perilaku
        </div>
        <div class="col-3 right">
            <i class="fa fa-plus"></i>
        </div>
    </div>
</h6>
<ul class="navbar-nav featureNav attitude">
    <li class="nav-item" id="rulePage">
        <a class="nav-link" href="{{ route('teacher/attitude/rule', $data['school']->code) }}">
            <i class="fa fa-gavel text-blue"></i> Peraturan
        </a>
    </li>
    <li class="nav-item" id="achievementPage">
        <a class="nav-link" href="{{ route('teacher/attitude/achievement', $data['school']->code) }}">
            <i class="fa fa-trophy text-blue"></i> Prestasi
        </a>
    </li>
    <li class="nav-item" id="violationPage">
        <a class="nav-link" href="{{ route('teacher/attitude/violation', $data['school']->code) }}">
            <i class="fa fa-exclamation-circle text-blue"></i> Pelanggaran
        </a>
    </li>
    <li class="nav-item" id="statisticPage">
        <a class="nav-link" href="{{ route('teacher/attitude/statistics', $data['school']->code) }}">
            <i class="fa fa-chart-bar text-blue"></i> Statistik
        </a>
    </li>
</ul>