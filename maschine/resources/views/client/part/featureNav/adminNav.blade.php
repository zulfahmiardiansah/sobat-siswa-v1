<hr class="my-3">
<h6 class="navbar-heading text-muted menuTrigger" onclick="openMenu('.school-data')">
    <div class="row">
        <div class="col-9">
            Data Dasar
        </div>
        <div class="col-3 right">
            <i class="fa fa-plus"></i>
        </div>
    </div>
</h6>
<ul class="navbar-nav featureNav school-data">
    <li class="nav-item" id="classPage">
        <a class="nav-link" href="{{ route('teacher/class', $data['school']->code) }}">
            <i class="fa fa-building text-blue"></i> Kelas
        </a>
    </li>
    <li class="nav-item" id="studentPage">
        <a class="nav-link" href="{{ route('teacher/student', $data['school']->code) }}">
            <i class="fa fa-user-graduate text-blue"></i> Siswa
        </a>
    </li>
    <li class="nav-item" id="teacherPage">
        <a class="nav-link" href="{{ route('teacher/teacher', $data['school']->code) }}">
            <i class="fa fa-chalkboard-teacher text-blue"></i> Guru
        </a>
    </li>
    <li class="nav-item" id="informationPage">
        <a class="nav-link" href="{{ route('teacher/information', $data['school']->code) }}">
            <i class="fa fa-book text-blue"></i> Sekolah
        </a>
    </li>
</ul>