<hr class="my-3">
<h6 class="navbar-heading text-muted menuTrigger" onclick="openMenu('.mading')">
    <div class="row">
        <div class="col-9">
            Mading Elektronik
        </div>
        <div class="col-3 right">
            <i class="fa fa-plus"></i>
        </div>
    </div>
</h6>
<ul class="navbar-nav featureNav mading">
    <li class="nav-item" id="explorePage">
        <a class="nav-link" href="{{ route('teacher/mading/explore', $data['school']->code) }}">
            <i class="fa fa-chalkboard text-blue"></i> Jelajahi
        </a>
    </li>
    
    <li class="nav-item" id="managePage">
        <a class="nav-link" href="{{ route('teacher/mading/manage', $data['school']->code) }}">
            <i class="fa fa-tasks text-blue"></i> Kelola
        </a>
    </li>
</ul>