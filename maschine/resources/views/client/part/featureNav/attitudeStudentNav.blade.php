<hr class="my-3">
<h6 class="navbar-heading text-muted menuTrigger" onclick="openMenu('.attitude')">
    <div class="row">
        <div class="col-9">
            Evaluasi Perilaku
        </div>
        <div class="col-3 right">
            <i class="fa fa-plus"></i>
        </div>
    </div>
</h6>
<ul class="navbar-nav featureNav attitude">
    <li class="nav-item" id="achievementPage">
        <a class="nav-link" href="{{ route('student/attitude/achievement', $data['school']->code) }}">
            <i class="fa fa-trophy text-blue"></i> Prestasi
        </a>
    </li>
    <li class="nav-item" id="violationPage">
        <a class="nav-link" href="{{ route('student/attitude/violation', $data['school']->code) }}">
            <i class="fa fa-exclamation-circle text-blue"></i> Pelanggaran
        </a>
    </li>
</ul>