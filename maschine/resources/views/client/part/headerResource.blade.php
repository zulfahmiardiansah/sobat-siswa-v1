<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sobat Siswa | {{ $data['school']->name }}</title>
    <link href="{{ asset('public/img/brand/favicon.png') }}" rel="icon" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ asset('public/adminAssets/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ asset('public/adminAssets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}"
        rel="stylesheet">
    <link href="{{ asset('public/adminAssets/css/argon.css?v=1.0.0') }}" rel="stylesheet">
    <link href="{{ asset('public/adminAssets/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/adminAssets/css/weather-icons.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/adminAssets/vendor/jquery.scrollbar/jquery.scrollbar.css') }}" />
    <link rel="icon" type="image/png" href="{{ asset('public/loginAssets/img/ros2.png') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/commonAssets/css/fakeLoader.css') }}">
    <script src="{{ asset('public/adminAssets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/commonAssets/js/fakeLoader.js') }}"></script>
    <style>
        body {
            background-image: url({{ asset('public/adminAssets/img/edu.png') }});
            background-size: 30%;
        }

        .swal2-popup {
            width: 20em !important;
        }

        .swal2-icon {
            margin: 1em auto 1em !important;
        }

        .swal2-confirm.swal2-styled {
            transform: scale(0.9);
        }
        
        .select2 {
            width: 100% !important;
            display: block;
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            padding: 8px 15px;
            font-size: 11pt;
            height: 45px !important;
        }

        .select2-container .select2-selection--single {
            height: 45px !important;
            border: none;
            box-shadow: 0 1px 3px rgba(50, 50, 93, .15), 0 1px 0 rgba(0, 0, 0, .02);
            border-radius: 5px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 26px;
            position: absolute;
            top: 50%;
            right: 10px;
            width: 20px;
            transform: translateY(-50%);
        }

        .select2-dropdown {
            border: none;
            box-shadow: 0 1px 3px rgba(50, 50, 93, .15), 0 1px 0 rgba(0, 0, 0, .02);
        }

        .select2-results__option {
            padding: 6px 15px;
        }
        
        body {
            position: relative;
            height: 100%;
        }

        .appLogo {
            height: 90px;
            max-height: none !important;
        }

        #sidenav-main {
            overflow-y: visible;
            position: relative;
            float: left;
            box-shadow: none !important;
        }

        @media(max-width: 768px) {
            .appLogo {
                height: 50px;
            }

            #sidenav-main {
                position: relative;
                float: none;
            }
        }

        .bgWhite {
            background-color: #fff;
            position: fixed;
            height: 100vh;
            width: 250px;
            left: 0;
            top: 0;
            z-index: 1;
            box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;
        }

        #sidenav-main .container-fluid {
            z-index: 2;
            position: relative;
        }

        @media (max-width: 768px) {
            .bgWhite {
                display: none;
            }
        }

        .nav-item.active {
            background-color: rgb(250,250,250);
        }

        div.header {
            background-size: cover !important;
            opacity: 0;
            background: none !important;
            background-image: url(<?php echo asset('public/adminAssets/img/cube.jpg') ?>) !important;
            animation: crossFade 1s forwards;
            background-position: bottom !important;
        }

        div.main-content {
            animation-delay: 0.3s;
            opacity: 0;
            animation: crossFade 1s forwards;
            animation-delay: .5s;
        }
        
        @keyframes crossFade {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        .card-profile-image img {
            padding: 20px;
            background-color: #fff;
            border-radius: 30%;
        }
        
        .menuTrigger {
            cursor: pointer;
            transition: .15s;
        }

        .menuTrigger:hover {
            color: #3a5aa7 !important;
        }

        .featureNav {
            display: none;
        }

        .datepicker {
            min-width: 130px;
        }

        .datepicker .month {
            height: 35px;
            line-height: 35px;
            font-size: 9pt;
        }

        .datepicker .month.active {
            background: #06326f;
            font-weight: bold;
        }

        .datepicker .datepicker-switch {
            font-weight: bold;
        }

        .swal2-image {
            width: 200px;
            max-width: 100%;
            margin: 1em;
        }

        footer.footer .row {
            margin: 0px;
        }

        footer.footer {
            padding: 1em;
            padding-top: 1.1em;
            color: white !important;
            background-color: transparent;
        }
        
        @media (min-width: 768px) {
            .fakeLoader {
                display: none;
            }
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet"> 
    <link rel="manifest" href="{{ asset('public/manifest/manifest-' . $data['school']->code . '.json') }}">
</head>
<body>
    <div class="fakeLoader"></div>
    <script>    
            $.fakeLoader({
                timeToHide:1200,
                bgColor:"#fff",
                spinner:"spinner7"
            });
    </script>
    @include('client/part/sidebar')
