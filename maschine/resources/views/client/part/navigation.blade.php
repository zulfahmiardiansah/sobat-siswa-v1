@if ($data['school']['session']->role < 3)
    <!------- Navigasi Utama ------->
    <h6 class="navbar-heading text-muted">Navigasi Utama</h6>
    <ul class="navbar-nav">
        <li class="nav-item" id="dashboardPage">
            <a class="nav-link" href="{{ route('teacher/dashboard', $data['school']->code) }}">
                <i class="fa fa-home text-blue"></i> Dasbor
            </a>
        </li>
    </ul>
    @if ($data['school']['session']->role < 1)
        @include("client.part.featureNav.adminNav")
    @endif
    @foreach (json_decode($data['school']->feature) as $single)
        @include("client.part.featureNav." . $single->code . "TeacherNav")
    @endforeach
    <hr class="my-3">
    <h6 class="navbar-heading text-muted menuTrigger" onclick="openMenu('.helpdesk')">
        <div class="row">
            <div class="col-9">
                Bantuan
            </div>
            <div class="col-3 right">
                <i class="fa fa-plus"></i>
            </div>
        </div>
    </h6>
    <ul class="navbar-nav featureNav helpdesk">
        <li class="nav-item" id="docPage">
            <a class="nav-link" href="https://revolution-of-school.gitbook.io/sobat-siswa/" target="_blank">
                <i class="fa fa-question-circle text-blue"></i> Dokumentasi
            </a>
        </li>
    </ul>
@else
    <!------- Navigasi Utama ------->
    <h6 class="navbar-heading text-muted">Navigasi Utama</h6>
    <ul class="navbar-nav">
        <li class="nav-item" id="dashboardPage">
            <a class="nav-link" href="{{ route('student/dashboard', $data['school']->code) }}">
                <i class="fa fa-home text-blue"></i> Dasbor
            </a>
        </li>
        <li class="nav-item" id="teacherListPage">
            <a class="nav-link" href="{{ route('student/teacher', $data['school']->code) }}">
                <i class="fa fa-book-reader text-blue"></i> Kontak Guru
            </a>
        </li>
    </ul>
    @foreach (json_decode($data['school']->feature) as $single)
        @include("client.part.featureNav." . $single->code . "StudentNav")
    @endforeach
@endif
