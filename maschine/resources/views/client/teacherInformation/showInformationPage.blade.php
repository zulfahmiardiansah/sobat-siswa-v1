@include('client/part/headerResource')


<!-- Header -->
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
    style="min-height: 450px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-7"></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-lg-8 col-md-10">
                <h1 class="display-2 text-white">Hai, <b>{{ $data['school']['session']->name }}</b></h1>
                <p class="text-white mt-0 mb-5">Di halaman ini, anda bisa melihat dan mengubah informasi mengenai sekolahmu. Kamu juga bisa melihat plan yang digunakan sekolahmu.</p>
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
    <div class="row mb-5">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="{{ asset($data['school']->logo) }}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="tel:{{ $data['school']->phone }}" style="color: white;" class="btn btn-sm bg-orange mr-4">Telp</a>
                        <a href="mailto:{{ $data['school']->email }}" class="btn btn-sm btn-default float-right">E-Mail</a>
                    </div>
                </div>
                <div class="card-body pt-0 pt-md-4">
                    <div class="row">
                        <div class="col">
                            <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                <div>
                                    <span class="heading">{{ $data['school']->limit }}</span>
                                    <span class="description">Batas</span>
                                </div>
                                <div>
                                    <span class="heading">{{ date("d M Y", strtotime($data['school']->lifetime)) }}</span>
                                    <span class="description">Masa Pelayanan</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <h2>
                            {{ $data['school']->name }}
                        </h2>
                        <div class="h4 font-weight-300">
                            {{ $data['school']->address }}
                        </div>
                        <hr class="my-4" />
                        <h2>
                            Plan Yang Digunakan
                        </h2>
                        <div class="h4 font-weight-300">
                            {{ $data['school']->plan_name }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Data Sekolah</h3>
                        </div>
                        <div class="col-4 text-right">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Nama
                                        </label>
                                        <input required="" name="name" value="{{ $data['school']->name }}" type="text" 
                                            class="form-control form-control-alternative" placeholder="Masukkan Nama" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            No. Telp
                                        </label>
                                        <input required="" name="phone" value="{{ $data['school']->phone }}" type="phone" 
                                            class="form-control form-control-alternative" placeholder="Masukkan No. Telp" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            E-Mail
                                        </label>
                                        <input required="" name="email" value="{{ $data['school']->email }}" type="email" 
                                            class="form-control form-control-alternative" placeholder="Masukkan E-Mail" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Ubah Logo
                                        </label>
                                        <input type="file" name="logo" style="padding: 10px 0px;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Alamat
                                        </label>
                                        <textarea required="" name="address" class="form-control form-control-alternative" placeholder="Masukkan Alamat" >{{ $data['school']->address }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-xs btn-primary" style="transform: scale(.9)">Ubah</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("School Data");
    $("#informationPage").addClass("active");
    $(".school-data.featureNav").show(300);
</script>
