@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-12 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-list text-primary"></i>&nbsp;&nbsp; Kelola Papan</h3>
                        </div>
                        <div class="col-right">
                                <a href="./manage/add">
                                    <button class="btn btn-sm btn-primary">
                                        Tambah
                                    </button>
                                </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table-striped table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>
                                    Tanggal Dibuat
                                </th>
                                <th>
                                    Dibuat Oleh
                                </th>
                                <th>
                                    Judul Papan
                                </th>
                                <th>
                                    Jenis Papan
                                </th>
                                <th>
                                    Komentar
                                </th>
                                
                                <th>
                                    Aksi
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['content'] as $single)
                                <tr>
                                    <td>
                                        Pukul {{ date("H.i, d M Y", strtotime($single->created_at)) }}
                                    </td>
                                    <td>
                                        {{ $single->created_by }}
                                    </td>
                                    <td>
                                        {{ $single->short_desc }}
                                    </td>
                                    <td>
                                        @if ($single->type == 1)
                                            Pengumuman
                                        @elseif ($single->type == 2)
                                            Berita
                                        @elseif ($single->type == 3)
                                            Ekstrakulikuler
                                        @endif
                                    </td>
                                    <td>
                                        <span class="mr-2"><i class="fa fa-comment"></i>&nbsp; {{ $single->comment ? $single->comment : 0  }}</span>
                                    </td>
                                    
                                    <td>
                                        <a href="./manage/edit/{{ $single->id }}">
                                            <button class="btn btn-success btn-sm btn-icon-only">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </a>
                                        
                                        &nbsp;
                                        <a href="./manage/delete/{{ $single->id }}">
                                            <button class="btn btn-danger btn-sm btn-icon-only">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a>           
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['content']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
                <div class="card-footer py-4 text-center">
                    {{ $data['content']->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')


<script>
    $("#pageTitle").html("Kelola Mading");
    $(".mading.featureNav").show(300);
    $("#managePage").addClass("active");
</script>