@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-12 col-sm-12 col-xs-12 mb-5">
            <form action="" method="POST" enctype="multipart/form-data"  onsubmit="$('#long_desc').val($('.ql-editor').html()); $(this).submit();">
                {{ csrf_field() }}            
                <div class="card shadow">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><i class="fa fa-plus-circle text-primary"></i>&nbsp;&nbsp; Tambah Papan</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body bg-secondary">
                        <div class="form-group">
                            <label>
                                Judul *
                            </label>
                            <input type="text" required="" class="form-control form-control-alternative" name="short_desc" placeholder="Masukkan Judul Papan">
                        </div>
                        <div class="form-group">
                            <label>
                                Jenis *
                            </label>
                            <select name="type" class="form-control form-control-alternative" required="">
                                <option value="1">
                                    - Pilih Jenis  -
                                </option>
                                <option value="1">
                                    Pengumuman
                                </option>
                                <option value="2">
                                    Berita
                                </option>
                                <option value="3">
                                    Ekstrakulikuler
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Isi
                            </label>
                            <input type="hidden" name="long_desc" id="long_desc" required="">
                            <div id="editor" class="bg-white"> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" style="overflow: hidden">
                                    <label>
                                        Gambar Sampul
                                    </label>
                                    <input type="file" style="transform: scale(.9); margin-left: -15px;" class="d-block" name="thumbnail_img">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="overflow: hidden">
                                    <label>
                                        Gambar Utama
                                    </label>
                                    <input type="file" style="transform: scale(.9); margin-left: -15px;" class="d-block" name="feature_img">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="overflow: hidden">
                                    <label>
                                        Lampiran
                                    </label>
                                    <input type="file" style="transform: scale(.9); margin-left: -15px;" class="d-block" name="attachment">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6 d-none d-md-block">
                            <a href="{{ route('teacher/mading/manage', $data['school']->code) }}">                            
                                    <button style="transform:scale(0.9)" type="button" class="btn btn-danger">
                                        <i class="fa fa-chevron-left"></i>&nbsp; Kembali
                                    </button>
                                </a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button style="transform:scale(0.9)" type="submit" class="btn btn-primary ">
                                    <i class="fa fa-save"></i>&nbsp; Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@include('client/part/footerResource')


<script>
    $("#pageTitle").html("Kelola");
    $(".mading.featureNav").show(300);
    $("#managePage").addClass("active");
</script>

<script>
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
    quill.root.setAttribute('spellcheck', false)
</script>