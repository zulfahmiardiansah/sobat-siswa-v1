@include('client/part/headerResource')

<style>
    .card {
        width: 225px;
        margin: 10px;
        padding: 10px;
        background-color: transparent;
    }

    .card:hover {
        transform: translateY(-5px);
        box-shadow: 10px 0px 3px rgba(0,0,0,0.3);
    }

    .card .avatar {
        width: 150px;
        height: 150px;
        padding: 0px;
        margin: auto;
        display: block;
        border-radius: 3px;
        background-size: cover;
    }

    .clear {
        clear: both;
    }
    
    .bottomContact ul.pagination {
        display: inline-block;
    }

    .bottomContact ul.pagination li {
        display: inline-block;
    }

    .contactRow {
        width: 720px;
        max-width: 100%;
        margin: auto;
    }

    .contactRow a {
        display: inline-block;
    }

    @media (max-width: 500px) {
        .contactRow .card {
        transform: scale(.9);
        margin: 0px auto;
        }
        .contactRow .row a {
            margin: auto;
        }
    }

    .row .form-control {
        padding-right: 50px;
    }

    .row .form-group {
        max-width: 100%;
        position: relative;
        width: 500px;
        margin: auto;
    }

    .row button.searchButton {
        position: absolute;
        right: 0;
        top: 0;
        margin: 9px;
    }

</style>

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7 mb-5">
    <div class="row  mb-3">
        <div class="col-lg-12 text-center">
            <h1 class="text-white" style="font-family: 'Courgette', cursive; font-size: 2em;">
                Mading {{ $data['school']->name }}
            </h1>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-lg-12">
            <form action="" mehod="GET">
                <div class="form-group">
                    @if (isset($_GET['name']))
                        <input value="{{ $_GET['name'] }}" name="name" type="text" class="form-control form-control-alternative" placeholder="Ingin mencari lebih khusus ?">
                    @else
                        <input name="name" type="text" class="form-control form-control-alternative" placeholder="Ingin mencari lebih khusus ?">
                    @endif
                    <button class="btn btn-sm btn-primary searchButton">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="contactRow">
        <div class="row">
            <?php $number = 0 ?>
            @foreach ($data['content'] as $single)
                <a href="./explore/read/{{ $single->id }}">
                    <div class="card shadow text-center">
                        <div class="card-header border-0">
                            <div class="avatar" style="background-image: url({{ asset($single->thumbnail_img) }})">
                            </div>
                        </div>
                        <div style="background-color: white;">
                            <h4>
                                {{ $single->short_desc }}
                                <small class="d-block mt-2">
                                    Dibuat oleh <b>{{ $single->created_by }}</b>
                                    <span class="d-block mt-1"><i class="fa fa-clock mr-2"></i>{{ date("H.i, d M Y", strtotime($single->created_at)) }}</span>
                                    <span class="d-block mt-1"><i class="fa fa-comment mr-2"></i>{{ $single->comment ? $single->comment : 0  }} Komentar</span>
                                </small>
                            </h4>
                        </div>
                    </div>
                </a>
                <?php $number++ ?>
                @if ($number == 4)
                    </div>    
                    <div class="row">
                    <?php $number = 0 ?>
                @endif
            @endforeach
            
            @if (count($data['content']) == 0)
                <div class="card shadow text-center" style="display: block; margin: auto;">
                    <div class="card-body bg-white">
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 230px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    </div>
                </div>
            @endif
        </div>
        @if (!isset($_GET['name']))
        <div class="row mt-4">
            <div class="col-md-12 bottomContact">
                <div class="text-center">
                    {{ $data['content']->links() }}
                </div>
            </div>
        </div>
        @else
        <div class="row mt-4">
            <div class="col-md-12 bottomContact">
                <div class="text-center">
                    <a href="{{ route('student/mading/explore', $data['school']->code) }}">
                        <button class="btn btn-default" style="transform: scale(.9)">
                            Kembali
                        </button>
                    </a>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>


@include('client/part/footerResource')


<script>
    $("#pageTitle").html("Jelajahi Mading");
    $(".mading.featureNav").show(300);
    $("#explorePage").addClass("active");
</script>