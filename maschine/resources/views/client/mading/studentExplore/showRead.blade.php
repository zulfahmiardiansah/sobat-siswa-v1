@include('client/part/headerResource')

<style>
    .card-body p {
        margin-bottom: 0px;
        text-align: justify;
        font-size: 0.9em;
    }

    .row .form-control {
        padding-right: 50px;
    }

    .row .form-group {
        max-width: 100%;
        position: relative;
        margin: auto;
    }

    .row button.searchButton {
        position: absolute;
        right: 0;
        top: 0;
        margin: 9px;
    }

    .comment .card {
        border: 0;
        box-shadow: 0 1px 3px rgba(50, 50, 93, .15), 0 1px 0 rgba(0, 0, 0, .02);
    }

    .comment .card p {
        font-size: .83em;
    }
</style>

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-12 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header">
                    <h2>
                        {{ $data['content']->short_desc }}
                        <small class="d-block mt-2" style="font-size: .7em;">
                            <span class="d-inline-block mr-3">Dibuat oleh <b>{{ $data['content']->created_by }}</b></span>
                            <span class="d-inline-block mr-3"><i class="fa fa-clock mr-2"></i>{{ date("H.i, d M Y", strtotime($data['content']->created_at)) }}</span>
                            <span class="d-inline-block mr-3"><i class="fa fa-comment mr-2"></i>{{ $data['content']->comment ? $data['content']->comment : 0  }} Komentar</span>
                        </small>
                    </h2>
                </div>
                @if ($data['content']->feature_img != null || $data['content']->feature_img != '')
                    <div class="card-body" style="border-bottom: 1px solid rgba(0, 0, 0, .05);">
                        <img src="{{ asset($data['content']->feature_img) }}" style="max-width: 100%; width: 600px;" class="img-thumbnail d-block">
                    </div>
                @endif
                <div class="card-body" style="border-bottom: 1px solid rgba(0, 0, 0, .05);">
                    <?php echo $data['content']->long_desc ?>
                </div>
                @if ($data['content']->attachment != null || $data['content']->attachment != '')
                    <div class="card-body" style="border-bottom: 1px solid rgba(0, 0, 0, .05);">
                        <a href="{{ asset($data['content']->attachment) }}"><i class="fa fa-file"></i> &nbsp;&nbsp;Download lampirannya disini.</a>
                    </div>
                @endif
                <div class="card-body bg-secondary comment">
                    <div class="form-group">
                        <form action="" method="POST">
                            <input required="" name="comment" type="text" class="form-control form-control-alternative" placeholder="Ingin bertanya atau memberikan komentar ?">
                            <button class="btn btn-sm btn-primary searchButton">
                                <i class="fa fa-paper-plane"></i>
                            </button>
                            {{csrf_field()}}
                        </form>
                    </div>
                    @foreach ($data['comment'] as $single)
                    <div class="card mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div style="width: 100px;">
                                    <div class="Image placeholder" style="border-radius: 50%; margin: auto; width: 55px; height: 55px; background: url({{ asset( $single->profile_picture ) }}); background-size: cover"></div>                                
                                </div>
                                <div class="px-3" style="width: calc(100% - 100px)">
                                    <h4>
                                        {{ $single->name }}
                                    </h4>
                                    <p>{{ $single->comment }}</p>
                                    <h4 class="mb-0">
                                        <small class="d-block mt-3" style="font-size: .8em;">
                                            <span class="d-inline-block mr-3"><i class="fa fa-clock mr-2"></i>{{ date("H.i, d M Y", strtotime($single->created_at)) }}</span>
                                        </small>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('student/mading/explore', $data['school']->code) }}">                            
                                <button style="transform:scale(0.9)" type="button" class="btn btn-danger">
                                    <i class="fa fa-chevron-left"></i>&nbsp; Kembali
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')


<script>
    $("#pageTitle").html("Jelajahi Mading");
    $(".mading.featureNav").show(300);
    $("#explorePage").addClass("active");
</script>