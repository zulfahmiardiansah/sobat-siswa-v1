@include('client/part/headerResource')

<style>
    .card {
        width: 200px;
        margin: 10px;
        padding: 10px;
        background-color: transparent;
    }

    .card .avatar {
        width: 120px;
        height: 120px;
        border-radius: 50%;
        padding: 0px;
        margin: auto;
        display: block;
        background-size: cover;
    }

    .clear {
        clear: both;
    }
    
    .bottomContact ul.pagination {
        display: inline-block;
    }

    .bottomContact ul.pagination li {
        display: inline-block;
    }

        .contactRow {
    width: 880px;
    max-width: 100%;
    margin: auto;
    }

    @media (max-width: 500px) {
        .contactRow .card {
    transform: scale(.9);
    margin: 0px auto;
    }
    }

    .row .form-control {
        padding-right: 50px;
    }

    .row .form-group {
    max-width: 100%;
    position: relative;
    width: 500px;
    margin: auto;
    }

    .row button.searchButton {
        position: absolute;
        right: 0;
        top: 0;
        margin: 9px;
    }

</style>

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--8 mb-5">
    <div class="contactRow">
        <div class="row mb-3">
                @if (isset($_GET['name']))
                <div class="col-md-12 mb--1">
                    <h4 class="mt-3 text-center text-white">
                        Sekitar {{ count($data['teacher']) }} <b>hasil pencarian</b> untuk '{{ $_GET['name'] }}'
                    </h4>
                </div>
                @endif
            <div class="col-md-12 mt-4">
                <form action="" mehod="GET">
                    <div class="form-group">
                        @if (isset($_GET['name']))
                            <input value="{{ $_GET['name'] }}" name="name" type="text" class="form-control form-control-alternative" placeholder="Ingin mencari lebih khusus ?">
                        @else
                            <input name="name" type="text" class="form-control form-control-alternative" placeholder="Ingin mencari lebih khusus ?">
                        @endif
                        <button class="btn btn-sm btn-primary searchButton">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <?php $number = 0 ?>
            @foreach ($data['teacher'] as $single)
                <div class="card shadow text-center">
                    <div class="card-header border-0">
                        <div class="avatar" style="background-image: url({{ asset($single->picture) }})">
                        </div>
                    </div>
                    <div style="background-color: white;">
                        <table class="table" style="border-bottom: 1px solid #e9ecef;">
                            <tr>
                                <th  style="font-size: 10.5pt; white-space: normal;">
                                    {{ $single->name }}
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    @if ($single->phone != null && $single->phone != '')
                                    <a href="tel:{{ $single->phone }}">
                                        <button title="{{ $single->phone }}" class="btn btn-success btn-sm btn-outline">
                                            <i class="fab fa-whatsapp" style="font-size: 14pt;"></i>   
                                        </button>
                                    </a>
                                    @endif
                                    &nbsp;
                                    @if ($single->email != null && $single->email != '')
                                    <a href="mailto:{{ $single->email }}">
                                        <button title="{{ $single->email }}" class="btn btn-info btn-sm btn-outline">
                                            <i class="fa fa-envelope" style="font-size: 14pt;"></i>   
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php $number++ ?>
                @if ($number == 4)
                    </div>    
                    <div class="row">
                    <?php $number = 0 ?>
                @endif
            @endforeach
            
            @if (count($data['teacher']) == 0)
                <div class="card shadow text-center" style="display: block; margin: auto;">
                    <div class="card-body bg-white">
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 230px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    </div>
                </div>
            @endif
        </div>
        @if (!isset($_GET['name']))
        <div class="row mt-4">
            <div class="col-md-12 bottomContact">
                <div class="text-center">
                    {{ $data['teacher']->links() }}
                </div>
            </div>
        </div>
        @else
        <div class="row mt-4">
            <div class="col-md-12 bottomContact">
                <div class="text-center">
                    <a href="{{ route('student/teacher', $data['school']->code) }}">
                        <button class="btn btn-default" style="transform: scale(.9)">
                            Kembali
                        </button>
                    </a>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Kontak Guru");
    $("#teacherListPage").addClass("active");
</script>