<form method="POST" action="./student/add" enctype="multipart/form-data">
    {{ csrf_field() }}
	<div class="modal-body bg-secondary">
            <div class="form-group">
                  <label> NIS </label>
                  <input placeholder="Masukkan NIS" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="nis" required="" />
            </div>
            <div class="form-group">
                  <label> Nama </label>
                  <input placeholder="Masukkan Nama" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="name" required="" />
            </div>
            <div class="form-group">
                  <label> Tanggal Lahir </label>
                  <input autocomplete="off" type="date" class=" form-control-alternative form-control" name="date_birth"
                        required="" />
            </div>
            <div class="form-group">
                  <label> Tempat Lahir </label>
                  <input placeholder="Masukkan Tempat Lahir" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="place_birth" required="" />
            </div>
            <div class="form-group">
                  <label> Jenis Kelamin </label>
                  <select class=" form-control-alternative form-control" name="gender" required="">
                        <option value="Unknown">
                        - Pilih Jenis Kelamin -
                        </option>
                        <option value="Laki-laki">
                        Laki-laki
                        </option>
                        <option value="Perempuan">
                        Perempuan
                        </option>
                  </select>
            </div>
            <div class="form-group">
                  <label> Alamat </label>
                  <textarea placeholder="Masukkan Alamat" spellcheck="false" autocomplete="off"
                        class=" form-control-alternative form-control" name="address" required=""></textarea>
            </div>
            <div class="form-group">
                  <label> E-Mail </label>
                  <input placeholder="Masukkan E-Mail" autocomplete="off" type="email"
                        class=" form-control-alternative form-control" name="email" />
            </div>
            <div class="form-group">
                  <label> No. Telp </label>
                  <input placeholder="Masukkan No. Telp" autocomplete="off" type="phone"
                        class=" form-control-alternative form-control" name="phone" />
            </div>
            <div class="form-group">
                  <label> Password </label>
                  <input placeholder="Masukkan Password" autocomplete="off" type="text"
                        class=" form-control-alternative form-control" name="password" required="" />
            </div>
            <div class="form-group" id="containerClass">
                  <label> Kelas </label>
                  <select id="class_id" class=" form-control-alternative form-control" name="class_id" required="">
                        <option value="Unknown">
                        - Pilih Kelas -
                        </option>
                        @foreach ($data['class'] as $single)
                        <option value="{{ $single->id }}">
                        {{ $single->name }}
                        </option>
                        @endforeach
                  </select>
            </div>
            <div class="form-group">
                  <label> Gambar ( Opsional )</label>
                  <br>
                  <input placeholder="Masukkan Gambar" autocomplete="off" type="file" name="picture" />
            </div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-xs btn-secondary"
			data-dismiss="modal">Tutup</button>
		<button type="submit" class="btn btn-xs btn-primary">Simpan</button>
	</div>
</form>
<script>
      $("#class_id").select2({
        dropdownParent: $('#containerClass')
    });
</script>