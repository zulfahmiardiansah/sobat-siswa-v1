<table>
    <tr>
        <td>
            <b>
                NIS
            </b>
        </td>
        <td>
            <b>
                Nama
            </b>
        </td>
        <td>
            <b>
                Kelas
            </b>
        </td>
        <td>
            <b>
                Jenis Kelamin
            </b>
        </td>
        <td>
            <b>
                Tempat / Tanggal Lahir
            </b>
        </td>
        <td>
            <b>
                E-Mail
            </b>
        </td>
        <td>
            <b>
                No. Telp
            </b>
        </td>
        <td>
            <b>
                Alamat
            </b>
        </td>
    </tr>
    @foreach ($data['student'] as $single)
        <tr>
        <td>
            {{ $single->nis }}
        </td>
        <td>
            {{ $single->name }}
        </td>
        <td>
            {{ $single->class_name }}
        </td>
        <td>
            {{ $single->gender }}
        </td>
        <td>
            @if ($single->date_birth != null || $single->date_birth != '')
                {{ $single->place_birth }}, {{ date("d M Y", strtotime($single->date_birth)) }}
            @endif
        </td>
        <td>
            {{ $single->email }}
        </td>
        <td>
            {{ $single->phone }}
        </td>
        <td>
            {{ $single->address }}
        </td>
        </tr>
    @endforeach
</table>