@include('client/part/headerResource')

<div class="header bg-gradient-primary pb-7 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<?php
    $getParameter   =   "";
    if (isset($_GET)) :
        $getParameter   =   "?";
        foreach ($_GET as $single) :
            $getParameter   .=  array_search($single, $_GET) . '=' . $single . '&';
        endforeach;
    endif;
?>

<div class="container-fluid mt--7">
    <div class="row" style="margin-bottom: 1rem;">
        <div class="col-md-12 col-sm-12 col-xs-12 mb-5">
            <div class="card shadow">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0"><i class="fa fa-user-graduate text-primary"></i>&nbsp;&nbsp; 
                                Siswa 
                                @if (isset($_GET['search']))
                                : Data Pencarian
                                @endif
                            </h3>
                        </div>
                        <div class="col-right d-none d-md-block">
                            <span style="font-size: .8em">
                                <b>
                                    Kuota Tersisa :
                                </b>
                                {{  $data['school']->limit - $data['studentTotal'] }}
                            </span>
                            &nbsp;
                            <button class="btn btn-sm btn-primary" onclick="showAdd()">
                                Tambah
                            </button>
                            <a href="./student/export{{ $getParameter }}">
                                <button class="btn btn-sm btn-success">
                                    Export
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body d-sm-block d-md-none border border border-bottom-1">
                    <span style="font-size: 1em">
                        <div class="form-group text-center">
                            <b>
                                Kuota Tersisa :
                            </b>
                            {{  $data['school']->limit - $data['studentTotal'] }}
                        </div>
                    </span>
                    <div class="form-group">
                        <button style="display: block;" class="py-1 form-control btn btn-primary btn-xs form-control-sm bg-primary" onclick="showAdd()">
                            Tambah
                        </button>
                    </div>
                    <div class="form-group mb-2">
                        <a href="./student/export{{ $getParameter }}">
                            <button style="display: block;" class="py-1 form-control btn btn-success btn-xs form-control-sm bg-success">
                                Export
                            </button>
                        </a>
                    </div>
                </div>
                <div class="card-body border-0 bg-secondary">
                    <form action="" method="GET">
                        <div class="form-group mb-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="h5">
                                        Kata Kunci
                                    </label>
                                    @if (isset($_GET['keyword']))
                                        <input value="{{ $_GET['keyword'] }}" name="keyword" type="text" placeholder="Kata Kunci" class="form-control form-control-alternative form-control-sm">
                                    @else
                                        <input name="keyword" type="text" placeholder="Kata Kunci" class=" form-control-alternative form-control form-control-sm">
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label class="h5">
                                        Kelas
                                    </label>
                                    <select name="class_id" class="form-control form-control-alternative  form-control-sm">
                                        @if (isset($_GET['class_id']))
                                            @if ($_GET['class_id'] == 'All')
                                                <option value="All">
                                                    Semua
                                                </option>
                                            @else
                                                @foreach ($data['class'] as $single)
                                                    @if ($single->id == $_GET['class_id'])
                                                        <option value="{{ $single->id }}">
                                                            {{ $single->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif
                                        <option value="All">
                                            Semua
                                        </option>
                                        @foreach ($data['class'] as $single)
                                            <option value="{{ $single->id }}">
                                                {{ $single->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label class="h5">
                                        Jenis Kelamin
                                    </label>
                                    <select name="gender" class="form-control form-control-alternative  form-control-sm">
                                        @if (isset($_GET['gender']))
                                            @if ($_GET['gender'] == 'All')
                                                <option value="All">
                                                    Semua
                                                </option>
                                            @else
                                                <option value="{{ $_GET['gender'] }}">
                                                    {{ $_GET['gender'] }}
                                                </option>
                                            @endif
                                        @endif
                                        <option value="All">
                                            Semua
                                        </option>
                                        <option value="Laki-laki">
                                            Laki-laki
                                        </option>
                                        <option value="Perempuan">
                                            Perempuan
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label style="margin-bottom: .4rem">&nbsp;</label>
                                    <input type="hidden" name="search" value="true">
                                    <button class="py-1 form-control btn btn-primary btn-xs form-control-sm bg-primary" style="overflow: hidden">
                                        <i class="fa fa-search text-white"></i>&nbsp;<span class="h5 text-white">Cari</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="8%">Gambar</th>
                                <th scope="col" width="12%">NIS</th>
                                <th scope="col" width="22%">Nama</th>
                                <th scope="col">Jenis Kelamin</th>
                                <th scope="col" width="10%">Kelas</th>
                                <th scope="col" class="text-center" width="25%">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['student'] as $single)
                                <tr>
                                    <td>
                                        <div class="media align-items-center">
                                            <a href="" class="avatar rounded-circle mr-3" style="background-color: transparent;">
                                                <div class="Image placeholder" style="width: 40px; height: 40px; background: url({{ asset( ($single->picture) ? $single->picture : 'public/adminAssets/img/default.png'  ) }}); background-size: cover"></div>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $single->nis }}
                                    </td>
                                    <td>
                                        {{ $single->name }}
                                    </td>
                                    <td>
                                        {{ $single->gender }}
                                    </td>
                                    <td>
                                        {{ $single->class_name }}
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-success btn-xs btn-icon-only" onclick="showEdit({{ $single->id }})">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        <a href="./student/delete/{{ $single->id }}">
                                            <button class="btn btn-danger btn-xs btn-icon-only">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (count($data['student']) == 0)
                        <img src="{{ asset('public/adminAssets/img/empty.png') }}" style="width: 200px; max-width: 100%; padding: 20px 20px 10px 20px; margin: 20px auto 0px auto; display: block; box-sizing: border-box;">
                        <h3 class="text-center" style="margin-bottom: 35px; color: #2e576d">
                            <b>
                                Kosong
                            </b>
                        </h3>
                    @endif
                </div>
                @if (!isset($_GET['search']))
                    <div class="card-footer py-4 text-center" style="overflow-x:auto">
                        {{ $data['student']->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Data Siswa");
    $("#studentPage").addClass("active");
    $(".school-data.featureNav").show(300);
    function showAdd() {
        $("#modalTitle").html("Menambah Data Siswa");
        $("#modalContent").load("{{ route('teacher/student', $data['school']->code) .'/add' }}");
        $("#modal").modal("show");
    }
    function showEdit(e) {
        $("#modalTitle").html("Mengubah Data Siswa");
        $("#modalContent").load("{{ route('teacher/student', $data['school']->code) .'/edit/' }}" + e);
        $("#modal").modal("show");
    }
</script>