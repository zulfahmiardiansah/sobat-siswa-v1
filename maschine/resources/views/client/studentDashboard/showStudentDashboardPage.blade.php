@include('client/part/headerResource')

<style>
    .profileShow.form-control.form-control-alternative {
        line-height: calc(1.5rem + 2px);
        height: auto;
    }
</style>

<!-- Header -->
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
    style="min-height: 400px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-7"></span>    
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-lg-8 col-md-10">
                <!-- <img src="{{ asset('public/adminAssets/img/mascot/twinkle-1.svg') }}" alt=""> -->
                <h1 class="display-2 text-white">Hai, <b>{{ explode(" ", $data["school"]["session"]->name)[0] }}</b></h1>
                <p class="text-white mt-0 mb-5">Selamat datang di <b>Sobat Siswa</b>. Berikut merupakan biodatamu dan informasi mengenai sekolahmu. Jelajahi berbagai fitur yang ada dengan membuka menu yang tersedia.</p>
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
    <div class="row mb-5">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="{{ asset($data['school']->logo) }}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="tel:{{ $data['school']->phone }}" style="color: white;" class="btn btn-sm bg-orange mr-4">Telp</a>
                        <a href="mailto:{{ $data['school']->email }}" class="btn btn-sm btn-default float-right">E-Mail</a>
                    </div>
                </div>
                <div class="card-body pt-0 pt-md-4">
                    <div class="row">
                        <div class="col">
                            <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                <div>
                                    <span class="heading">{{ $data['studentCount'] }}</span>
                                    <span class="description">Siswa</span>
                                </div>
                                <div>
                                    <span class="heading">{{ $data['classCount'] }}</span>
                                    <span class="description">Kelas</span>
                                </div>
                                <div>
                                    <span class="heading">{{ $data['teacherCount'] }}</span>
                                    <span class="description">Guru</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <h2>
                            {{ $data['school']->name }}
                        </h2>
                        <div class="h4 font-weight-300">
                            {{ $data['school']->address }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Biodata</h3>
                        </div>
                        <div class="col-4 text-right">
                            <button class="btn btn-sm btn-primary profileShow" onclick="showEdit()" type="button">
                                <i class="fa fa-edit"></i>&nbsp; Ubah
                            </button>
                            <button class="btn btn-sm btn-primary profileEdit" onclick="showEdit()" type="button">
                                <i class="fa fa-chevron-left"></i>&nbsp; Batal
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Nama
                                        </label>
                                        <div class="profileShow form-control form-control-alternative ">
                                            @if ($data['biodata']->name == null || $data['biodata']->name == '')
                                                <i style="color: #ddd">- Data Belum Terisi -</i>
                                            @endif
                                            {{ $data['biodata']->name }}
                                        </div>
                                        <input required="" name="name" value="{{ $data['biodata']->name }}" type="text" 
                                            class="form-control form-control-alternative profileEdit" placeholder="Masukkan Nama" readonly="" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Telp
                                        </label>
                                        <div class="profileShow form-control form-control-alternative ">
                                            @if ($data['biodata']->phone == null || $data['biodata']->phone == '')
                                                <i style="color: #ddd">- Data Belum Terisi -</i>
                                            @endif
                                            {{ $data['biodata']->phone }}
                                        </div>
                                        <input name="phone" value="{{ $data['biodata']->phone }}" type="phone" 
                                            class="form-control form-control-alternative profileEdit" placeholder="Masukkan No. Telp" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Tempat Lahir
                                        </label>
                                        <div class="profileShow form-control form-control-alternative ">
                                            @if ($data['biodata']->place_birth == null || $data['biodata']->place_birth == '')
                                                <i style="color: #ddd">- Data Belum Terisi -</i>
                                            @endif
                                            {{ $data['biodata']->place_birth }}
                                        </div>
                                        <input required="" name="place_birth" value="{{ $data['biodata']->place_birth }}" type="text" 
                                            class="form-control form-control-alternative profileEdit" placeholder="Masukkan Tempat Lahir" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Tanggal Lahir
                                        </label>
                                        <div class="profileShow form-control form-control-alternative ">
                                            @if ($data['biodata']->date_birth == null || $data['biodata']->date_birth == '')
                                                <i style="color: #ddd">- Data Belum Terisi -</i>
                                            @endif
                                            {{ $data['biodata']->date_birth }}
                                        </div>
                                        <input required="" name="date_birth" value="{{ $data['biodata']->date_birth }}" type="date" 
                                            class="form-control form-control-alternative profileEdit" placeholder="Masukkan Tanggal Lahir" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            E-Mail
                                        </label>
                                        <div class="profileShow form-control form-control-alternative ">
                                            @if ($data['biodata']->email == null || $data['biodata']->email == '')
                                                <i style="color: #ddd">- Data Belum Terisi -</i>
                                            @endif
                                            {{ $data['biodata']->email }}
                                        </div>
                                        <input name="email" value="{{ $data['biodata']->email }}" type="email" 
                                            class="form-control form-control-alternative profileEdit" placeholder="Masukkan E-Mail" >
                                    </div>
                                </div>
                                <div class="col-lg-6 profileEdit">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Ubah Gambar
                                        </label>
                                        <div style="overflow-x: hidden;">
                                            <input type="file" name="picture" style="padding: 10px 0px; transform: scale(.9)">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 profileShow">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Alamat
                                        </label>
                                        <div class="profileShow form-control form-control-alternative ">
                                            @if ($data['biodata']->address == null || $data['biodata']->address == '')
                                                <i style="color: #ddd">- Data Belum Terisi -</i>
                                            @endif
                                            {{ $data['biodata']->address }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row profileEdit">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Alamat
                                        </label>
                                        <textarea required="" name="address" class="form-control form-control-alternative profileEdit" placeholder="Masukkan Alamat" >{{ $data['biodata']->address }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card-footer profileEdit">
                    <button class="btn btn-xs btn-success" style="transform: scale(.9)">
                        <i class="fa fa-save"></i>&nbsp; Simpan
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

@include('client/part/footerResource')

<script>
    $("#pageTitle").html("Dasbor");
    $("#dashboardPage").addClass("active");
    $(".profileEdit").hide();
    var editToggle = false;
    function showEdit () {
        if (editToggle) {
            $(".profileEdit").hide();
            $(".profileShow").show();
            editToggle = false;
        } else {
            $(".profileEdit").show();
            $(".profileShow").hide();
            editToggle = true;
        }
    }
</script>
