@include('console.part.headPart')

<div class="wrapper ">

    @include('console.part.navigationPart')

    <div class="main-panel">

        @include('console.part.nicknamePart')

        <div class="content" style="padding: 0px 15px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <div class="card">
                            <div class="card-header card-header-success" style="background: #3955a4; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)">
                                <h4 class="card-title" style="font-weight: normal;">School</h4>
                                <p class="card-category"> Our clients</p>
                            </div>
                            <div class="card-body" style="overflow-x: auto">
                                <div class="text-center">
                                    <button class="btn btn-info" onclick="showAddModal()">
                                        <i class="material-icons">add</i> &nbsp;Add Record
                                    </button>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th>
                                                    Information
                                                </th>
                                                <th width="30%">
                                                    Plan
                                                </th>
                                                <th width="10%">
                                                    Lifetime
                                                </th>
                                                <th width="25%" class="text-center">
                                                    Option
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['school'] as $single)
                                            <tr>
                                                <td>
                                                    <b>{{ $single->name }}</b>
                                                    <br>{{ $single->address }}
                                                </td>
                                                <td>
                                                    {{ $single->plan_name }}
                                                </td>
                                                <td>
                                                    <?php
                                                        $createdAt      =   date_create(date("Y-m-d"));
                                                        $diff           =   date_diff($createdAt, date_create($single->lifetime));
                                                        $limit_month    =   (int)$diff->format("%a");
                                                        if ($limit_month < 1) {
                                                            echo "<b style='color: red'>Expired</b>";
                                                        } else {
                                                            echo $limit_month . "+ Days";
                                                        }
                                                    ?>
                                                </td>
                                                <td class="text-center">
                                                    <button title="See Or Edit Information" class="btn btn-sm btn-warning" onclick="showEditModal({{ $single->id }})">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                    &nbsp;
                                                    <button title="Purchase A Plan" class="btn btn-sm btn-primary" onclick="showPurchaseModal({{ $single->id }})">
                                                        <i class="material-icons">add_to_photos</i>
                                                    </button>
                                                    &nbsp;
                                                    <a href="{{ url('' . $single->code . '/') }}" target="_blank">
                                                        <button title="See" class="btn btn-sm btn-success">
                                                            <i class="material-icons">remove_red_eye</i>
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                        {{ $data['school']->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('console.part.footerPart')

<style>
    ul.pagination {
        align-content: center;
        justify-content: center;
    }
</style>

<script>
    $("#school").addClass('active');
    function showAddModal () {
        $("#modal").modal("show");
        $("#modalTitle").html("Add School");
        $("#modalContent").load("{{ route('console/school') }}/add");
    }
    function showEditModal (e) {
        $("#modal").modal("show");
        $("#modalTitle").html("Edit School Information");
        $("#modalContent").load("{{ route('console/school') }}/edit/" + e);
    }
    function showPurchaseModal (e) {
        $("#modal").modal("show");
        $("#modalTitle").html("Purchase New Plan");
        $("#modalContent").load("{{ route('console/school') }}/purchase/" + e);
    }
</script>
