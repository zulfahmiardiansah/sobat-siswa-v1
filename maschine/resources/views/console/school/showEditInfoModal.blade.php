<form action="{{ route('console/school') }}/edit/{{ $data['school']->id }}" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="form-group">
            <label>
                Code
            </label>
            <input type="text" class="form-control" name="code" required="" autocomplete="off" value="{{ $data['school']->code }}">
        </div>
        <div class="form-group">
            <label>
                Name
            </label>
            <input type="text" class="form-control" name="name" required="" autocomplete="off" value="{{ $data['school']->name }}">
        </div>
        <div class="form-group">
            <label>
                Phone
            </label>
            <input type="phone" class="form-control" name="phone" required="" autocomplete="off" value="{{ $data['school']->phone }}">
        </div>
        <div class="form-group">
            <label>
                E-Mail
            </label>
            <input type="email" class="form-control" name="email" required="" autocomplete="off" value="{{ $data['school']->email }}">
        </div>
        <div class="form-group" style="margin-bottom: 0px !important;">
            <label>
                Address
            </label>
            <textarea class="form-control" name="address" required="" autocomplete="off" spellcheck="false">{{ $data['school']->address }}</textarea>
        </div>
        <div class="form-group">
            <label>
                Change Logo
            </label>
            <input type="file" class="form-control" name="logo" >
        </div>
        <div class="form-group">
            <label>
                Current Logo
            </label>
            <img src="{{ asset($data['school']->logo) }}" class="img-thumbnail logo">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-sm btn-primary">Save</button>
    </div>
</form>

<style>
    .form-group input[type="file"] {
        opacity: 1;
        position: static;
    }

    input.form-control {
        min-height: 50px !important;
    }

    textarea.form-control {
        padding: 15px 0px !important;
    }

    .form-group {
        margin-bottom: 20px;
    }

    .form-group:last-child {
        margin-bottom: 0px;
    }

    ol.feture li {
        font-size: 0.85em;
        color: #888;
    }

    ol.feture {
        padding-left: 30px;
    }

    p.feture {
        font-size: 0.85em;
        color: #888;
        margin-top: 10px;
        margin-bottom: 0px;
    }

    img.logo {
        display: block;
        width: 200px;
        margin: auto;
        max-width: 100%;
    }
</style>