<form action="{{ route('console/school') }}/add" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="form-group">
            <label>
                Code
            </label>
            <input type="text" class="form-control" name="code" required="" autocomplete="off">
        </div>
        <div class="form-group">
            <label>
                Name
            </label>
            <input type="text" class="form-control" name="name" required="" autocomplete="off">
        </div>
        <div class="form-group">
            <label>
                Phone
            </label>
            <input type="phone" class="form-control" name="phone" required="" autocomplete="off">
        </div>
        <div class="form-group">
            <label>
                E-Mail
            </label>
            <input type="email" class="form-control" name="email" required="" autocomplete="off">
        </div>
        <div class="form-group" style="margin-bottom: 0px !important;">
            <label>
                Address
            </label>
            <textarea class="form-control" name="address" required="" autocomplete="off" spellcheck="false"></textarea>
        </div>
        <div class="form-group">
            <label>
                Logo
            </label>
            <input type="file" class="form-control" name="logo" required="" >
        </div>
        <div class="form-group" style="margin-bottom: 0px;">
            <label>
                Starter Plan
            </label>
            <select class="featureSelect" name="plan_id" required="" onchange="showFeature(this)">
                <option value="">
                    - Choose One Plan -
                </option>
                @foreach ($data['plan'] as $single)
                    <option value="{{ $single->id }}">
                        {{ $single->name }}
                    </option>
                @endforeach
            </select>
            <div id="listfeature">
            </div>
        </div>
        <div class="form-group">
            <label>
                Proof Of Payment
            </label>
            <input type="file" class="form-control" name="proof" required="" >
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-sm btn-primary">Save</button>
    </div>
</form>

<style>
    .form-group input[type="file"] {
        opacity: 1;
        position: static;
    }

    input.form-control {
        min-height: 50px !important;
    }

    textarea.form-control {
        padding: 15px 0px !important;
    }

    .form-group {
        margin-bottom: 20px;
    }

    .form-group:last-child {
        margin-bottom: 0px;
    }

    ol.feture li {
        font-size: 0.85em;
        color: #888;
    }

    ol.feture {
        padding-left: 30px;
    }

    p.feture {
        font-size: 0.85em;
        color: #888;
        margin-top: 10px;
        margin-bottom: 0px;
    }
</style>

<script>
    $(document).ready(function() {
        $('.featureSelect').select2();
    });
    function showFeature (e) {
        var a = $(e).val();
        $("#listfeature").load("{{ route('console/school') }}/feature/" + a)
    }
</script>