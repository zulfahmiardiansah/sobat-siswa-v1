<p class="feture">
    This plan is valid for <b>{{ $data['plan']->limit_month }} months</b> and <b>{{ $data['plan']->limit }} student</b> at a price of <b>Rp. {{ number_format($data['plan']->price, 2) }}</b>
</p>
<ol class="feture" style="margin-bottom: 0px;">
    @foreach (json_decode($data['plan']->feature) as $single)
        <li>
            {{ $single->feature }}
        </li>
    @endforeach
</ol>
