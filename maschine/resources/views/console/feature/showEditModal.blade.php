<form action="{{ route('console/feature') }}/edit/{{ $data['feature']->id }}" method="POST">
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="form-group">
            <label>
                Code
            </label>
            <input type="text" class="form-control" name="code" required="" autocomplete="off" value="{{ $data['feature']->code }}">
        </div>
        <div class="form-group">
            <label>
                Description
            </label>
            <input type="text" class="form-control" name="feature" required="" autocomplete="off" value="{{ $data['feature']->feature }}">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-sm btn-primary">Save</button>
    </div>
</form>

<style>
    .form-control {
        min-height: 50px !important;
        padding: 20px 0px !important;
    }
    .form-group {
        margin-bottom: 20px;
    }
    .form-group:last-child {
        margin-bottom: 0px;
    }
</style>