@include('console.part.headPart')

<div class="wrapper ">

    @include('console.part.navigationPart')

    <div class="main-panel">

        @include('console.part.nicknamePart')

        <div class="content" style="padding: 0px 15px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <div class="card">
                            <div class="card-header card-header-success" style="background: #3955a4; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)">
                                <h4 class="card-title" style="font-weight: normal;">Revenue</h4>
                                <p class="card-category"> Revenue Log</p>
                            </div>
                            <div class="card-body" style="overflow-x: auto">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th width="15%">
                                                    Date
                                                </th>
                                                <th>
                                                    Description
                                                </th>
                                                <th width="15%">
                                                    Nominal
                                                </th>
                                                <th width="12%">
                                                    Opsi
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['log'] as $single)
                                                <tr>
                                                    <td>
                                                        {{ date("D, d M Y", strtotime($single->created_at)) }}
                                                    </td>
                                                    <td>
                                                        {{ $single->description }}
                                                    </td>
                                                    <td>
                                                        {{ "Rp. " . number_format($single->nominal, 0) }}
                                                    </td>
                                                    <td>
                                                        <a download="" href="{{ asset($single->proof) }}" class="btn btn-primary">
                                                            <i class="material-icons">archive</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                    {{ $data['log']->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('console.part.footerPart')

<style>
    ul.pagination {
        align-content: center;
        justify-content: center;
    }
</style>

<script>
    $("#log").addClass('active');
</script>