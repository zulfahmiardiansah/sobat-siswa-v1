<form action="{{ route('console/plan') }}/edit/{{ $data['plan']->id }}" method="POST">
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="form-group">
            <label>
                Name
            </label>
            <input type="text" class="form-control" name="name" required="" autocomplete="off" value="{{ $data['plan']->name }}">
        </div>
        <div class="form-group">
            <label>
                Price
            </label>
            <input type="number" class="form-control" name="price" required="" autocomplete="off" value="{{ $data['plan']->price }}">
        </div>
        <div class="form-group">
            <label>
                Limit On Number Of Students
            </label>
            <input type="number" class="form-control" name="limit" required="" autocomplete="off" value="{{ $data['plan']->limit }}">
        </div>
        <div class="form-group">
            <label>
                Limit On Month
            </label>
            <input type="number" class="form-control" name="limit_month" required="" autocomplete="off" value="{{ $data['plan']->limit_month }}">
        </div>
        <div class="form-group">
            <label>
                Description
            </label>
            <textarea class="form-control" name="description" required="" autocomplete="off" spellcheck="false">{{ $data['plan']->description }}</textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-sm btn-primary">Save</button>
    </div>
</form>

<style>
    input.form-control {
        min-height: 50px !important;
    }

    textarea.form-control {
        padding: 15px 0px !important;
    }

    .form-group {
        margin-bottom: 20px;
    }

    .form-group:last-child {
        margin-bottom: 0px;
    }

    .container {
        display: block;
        position: relative;
        padding-left: 30px;
        padding-top: 2px;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
    }

    .container:hover input~.checkmark {
        background-color: #ccc;
    }

    .container input:checked~.checkmark {
        background-color: #2196F3;
    }

    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    .container input:checked~.checkmark:after {
        display: block;
    }

    .container .checkmark:after {
        left: 7px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>