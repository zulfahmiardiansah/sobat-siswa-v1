@include('console.part.headPart')

<div class="wrapper ">

    @include('console.part.navigationPart')

    <div class="main-panel">

        @include('console.part.nicknamePart')

        <div class="content" style="padding: 0px 15px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <div class="card">
                            <div class="card-header card-header-success" style="background: #3955a4; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)">
                                <h4 class="card-title" style="font-weight: normal;">Plan</h4>
                                <p class="card-category"> Plans available and ready to use for clients</p>
                            </div>
                            <div class="card-body" style="overflow-x: auto">
                                <div class="text-center">
                                    <button class="btn btn-info" onclick="showAddModal()">
                                        <i class="material-icons">add</i> &nbsp;Add Record
                                    </button>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th width="30%" class="text-center">
                                                    Name
                                                </th>
                                                <th width="15%" class="text-center">
                                                    Price
                                                </th>
                                                <th width="10%" class="text-center">
                                                    Limit
                                                </th>
                                                <th width="25%" class="text-center">
                                                    Feature
                                                </th>
                                                <th width="20%" class="text-center">
                                                    Option
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['plan'] as $single)
                                                <tr>
                                                    <td> 
                                                        {{ $single->name }}
                                                    </td>
                                                    <td class="text-center"> 
                                                        Rp. {{ number_format($single->price, 2) }}
                                                        <br>/ {{ $single->limit_month }} Month
                                                    </td>
                                                    <td class="text-center">
                                                        {{ $single->limit }} Student
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            @foreach (json_decode($single->feature) as $single2)
                                                                <li>{{ $single2->feature }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </td>
                                                    <td class="text-center">
                                                        <button class="btn btn-sm btn-warning" onclick="showEditModal({{ $single->id }})">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        &nbsp;
                                                        <a href="{{ route('console/plan') }}/delete/{{ $single->id }}">
                                                            <button class="btn btn-sm btn-danger">
                                                                <i class="material-icons">delete_forever</i>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                        {{ $data['plan']->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('console.part.footerPart')

<style>
    ul.pagination {
        align-content: center;
        justify-content: center;
    }
</style>

<script>
    $("#plan").addClass('active');
    function showAddModal () {
        $("#modal").modal("show");
        $("#modalTitle").html("Add Plan");
        $("#modalContent").load("{{ route('console/plan') }}/add");
    }
    function showEditModal (e) {
        $("#modal").modal("show");
        $("#modalTitle").html("Edit Plan");
        $("#modalContent").load("{{ route('console/plan') }}/edit/" + e);
    }
</script>
