<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sobat Siswa Console</title> <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="{{ asset('public/consoleAssets/css/material-dashboard.css?v=2.1.1') }}" rel="stylesheet" />
    <link href="{{ asset('public/consoleAssets/demo/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/img/brand/favicon.png') }}" rel="icon" type="image/png">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <style>
        .swal2-popup {
            width: 20em !important;
        }

        .swal2-icon {
            margin: 1em auto 1em !important;
        }

        .swal2-confirm.swal2-styled {
            transform: scale(0.9);
        }

        .select2 {
            width: 100% !important;
            display: block;
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            padding: 3px 15px;
            font-size: 11pt;
        }

        .select2-container .select2-selection--single {
            height: 35px !important;
            border-color: #ccc;
            border-radius: 0px;
        }

        .main-panel {
            background-image: url("../public/consoleAssets/img/pattern.png");
        }

        #particle {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 1;
        }

        .content {
            position: relative;
            z-index: 2;
        }

        .sidebar {
            box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)
        }

        li.active a.nav-link {
            background: #3955a4 !important;
            box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)
        }

    </style>
    <script src="{{ asset('public/loginAssets/particleEffect/particles.min.js') }}"></script>
    <script>
        particlesJS.load('particle', '{{ asset("public/loginAssets/particleEffect/particle.json") }}', function () {
            console.log('callback - particles.js config loaded');
        });
    </script>
    <link rel="manifest" href="{{ asset('public/manifest/manifest-console.json') }}">
</head>

<body>
