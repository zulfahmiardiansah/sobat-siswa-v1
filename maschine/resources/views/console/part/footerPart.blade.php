    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="modalContent">
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('public/consoleAssets/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/core/bootstrap-material-design.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/moment.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/sweetalert2.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/jquery.bootstrap-wizard.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/bootstrap-selectpicker.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/jquery-jvectormap.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/nouislider.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/arrive.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/chartist.min.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/plugins/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/material-dashboard.js?v=2.1.1') }}" type="text/javascript"></script>
    <script src="{{ asset('public/consoleAssets/demo/demo.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/js/particles.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    @if (Session::has('error'))
    <script>
        Swal.fire({
            type: 'error',
            title: 'Oops !',
            text: '{{ Session::get('error') }}',
        })

    </script>
    @endif
    @if (Session::has('success'))
    <script>
        Swal.fire({
            type: 'success',
            title: 'Yeay !',
            text: '{{ Session::get('success') }}',
        })
    </script>
    @endif
</body>
</html>
