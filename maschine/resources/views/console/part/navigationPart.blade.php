<div class="sidebar" data-color="purple" data-background-color="white">

    <div class="logo">
        <a href="{{ route('console/dashboard') }}" class="simple-text logo-normal">
            <i class="material-icons" style="transform:translateY(-2px); margin-right:3px;">last_page</i>
            Dev <b>Console</b>
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li id="dashboard" class="nav-item ">
                <a class="nav-link" href="{{ route('console/dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li id="feature" class="nav-item ">
                <a class="nav-link" href="{{ route('console/feature') }}">
                    <i class="material-icons">layers</i>
                    <p>Feature</p>
                </a>
            </li>
            <li id="plan" class="nav-item ">
                <a class="nav-link" href="{{ route('console/plan') }}">
                    <i class="material-icons">devices</i>
                    <p>Plan</p>
                </a>
            </li>
            <li id="school" class="nav-item ">
                <a class="nav-link" href="{{ route('console/school') }}">
                    <i class="material-icons">school</i>
                    <p>School</p>
                </a>
            </li>
            <li id="log" class="nav-item ">
                <a class="nav-link" href="{{ route('console/log') }}">
                    <i class="material-icons">book</i>
                    <p>Revenue Log</p>
                </a>
            </li>
            <li id="" class="nav-item ">
                <a class="nav-link" href="{{ route('console/logout') }}">
                    <i class="material-icons">logout</i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </div>
</div>