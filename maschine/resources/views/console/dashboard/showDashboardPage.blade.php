@include('console.part.headPart')

<div class="wrapper ">

    @include('console.part.navigationPart')

    <div class="main-panel">

        @include('console.part.nicknamePart')
        
        <div class="content" style="padding: 0px 15px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-warning card-header-icon">
                                <div class="card-icon" style="background: #3955a4; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)">
                                    <i class="material-icons">school</i>
                                </div>
                                <p class="card-category">Client</p>
                                <h3 class="card-title">{{ $data['clientCount'] }}
                                    <small>Client</small>
                                </h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons">date_range</i>&nbsp; Until {{ date('M Y') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon" style="background: #3955a4; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)">
                                    <i class="material-icons">account_balance_wallet</i>
                                </div>
                                <p class="card-category">Revenue</p>
                                <h3 class="card-title">{{ floor($data['revenueCount'] / 1000) }} 
                                    <small>K</small>
                                </h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons">date_range</i>&nbsp; At {{ date('Y') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-danger card-header-icon">
                                <div class="card-icon" style="background: #3955a4; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)">
                                    <i class="material-icons">info_outline</i>
                                </div>
                                <p class="card-category">Issues</p>
                                <h3 class="card-title">-
                                    <small>
                                        Issue
                                    </small>
                                </h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons">local_offer</i>&nbsp; Tracked from Sobatsiswa.id
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-info card-header-icon">
                                <div class="card-icon" style="background: #3955a4; box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14)">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <p class="card-category">Suggestion</p>
                                <h3 class="card-title">-</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons">local_offer</i>&nbsp; Tracked from Sobatsiswa.id
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('console.part.footerPart')

<script>
    $("#dashboard").addClass('active');
</script>