<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sobat Siswa Console</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('public/img/brand/favicon.png') }}" rel="icon" type="image/png">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/consoleAssets/login/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/consoleAssets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/consoleAssets/login/vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/consoleAssets/login/vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/consoleAssets/login/vendor/select2/select2.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/consoleAssets/login/css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/consoleAssets/login/css/main.css') }}">
    <!--===============================================================================================-->
    <style>
        .swal2-popup {
            width: 20em !important;
        }

        .swal2-icon {
            margin: 1em auto 1em !important;
        }

        .swal2-confirm.swal2-styled {
            transform: scale(0.9);
        }
        
        #particle {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: -9999;
        }

        .mascot {
            max-width: 100%;
        }
    </style>
    
    <link rel="manifest" href="{{ asset('public/manifest/manifest-console.json') }}">
</head>

<body>
<div id="particle"></div>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" method="POST" action="">
                    <img src='{{ asset("public/adminAssets/img/mascot/iddle.png") }}' class="mascot">
                    <span class="login100-form-title">
                        Sobat Siswa Console
                    </span>
                    
                    {{ csrf_field() }}
                    <div class="wrap-input100 validate-input" data-validate="Username is required">
                        <input class="input100" type="text" name="username" placeholder="Username">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>

                    <div class="text-center p-t-12">
                        <span class="txt1">
                            Hanya Untuk Administrator.
                        </span>
                        <a class="txt2" href="#">
                            Pelanggaran Akan Ditindak Secara Hukum.
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <!--===============================================================================================-->
    <script src="{{ asset('public/consoleAssets/login/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('public/consoleAssets/login/vendor/bootstrap/js/popper.js') }}"></script>
    <script src="{{ asset('public/consoleAssets/login/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('public/consoleAssets/login/vendor/select2/select2.min.js') }}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('public/consoleAssets/login/vendor/tilt/tilt.jquery.min.js') }}"></script>
    <script>
        $('.js-tilt').tilt({
            scale: 1.1
        })

    </script>
    <!--===============================================================================================-->
    <script src="{{ asset('public/consoleAssets/login/js/main.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    @if (Session::has('error'))
    <script>
        Swal.fire({
            type: 'error',
            title: 'Oops !',
            text: '{{ Session::get('error') }}',
        })

    </script>
    @endif
    @if (Session::has('success'))
    <script>
        Swal.fire({
            type: 'success',
            title: 'Yeay !',
            text: '{{ Session::get('success') }}',
        })
    </script>
    @endif
    <script src="{{ asset('public/loginAssets/particleEffect/particles.min.js') }}"></script>
    <script>
        particlesJS.load('particle', '{{ asset("public/loginAssets/particleEffect/particle.json") }}', function () {
            console.log('callback - particles.js config loaded');
        });
    </script>
</body>

</html>
