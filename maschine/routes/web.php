<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
*/
Route::prefix("console")->group(function () {

    // UserAuth
        Route::get("", "Console\UserAuth@showLoginPage")
            ->name("console/login");
        Route::post("", "Console\UserAuth@processLoginPage");
        Route::get("logout", "Console\UserAuth@processLogout")
            ->name("console/logout");
    
    // Dashboard
        Route::get("dashboard", "Console\Dashboard@showDashboardPage")
            ->name("console/dashboard");

    // Feature
        Route::get("feature", "Console\FeaturePage@showFeaturePage")
            ->name("console/feature");
            Route::get("feature/add", "Console\FeaturePage@showAddModal");
            Route::post("feature/add", "Console\FeaturePage@processAdd");
            Route::get("feature/edit/{id}", "Console\FeaturePage@showEditModal");
            Route::post("feature/edit/{id}", "Console\FeaturePage@processEdit");
            Route::get("feature/delete/{id}", "Console\FeaturePage@processDelete");

    // Plan
        Route::get("plan", "Console\PlanPage@showPlanPage")
        ->name("console/plan");
            Route::get("plan/add", "Console\PlanPage@showAddModal");
            Route::post("plan/add", "Console\PlanPage@processAdd");
            Route::get("plan/delete/{id}", "Console\PlanPage@processDelete");
            Route::get("plan/edit/{id}", "Console\PlanPage@showEditModal");
            Route::post("plan/edit/{id}", "Console\PlanPage@processEdit");

    // School
        Route::get("school", "Console\SchoolPage@showSchoolPage")
            ->name("console/school");
            Route::get("school/add", "Console\SchoolPage@showAddModal");
            Route::post("school/add", "Console\SchoolPage@processAdd");
            Route::get("school/feature/{id}", "Console\SchoolPage@showFeature");
            Route::get("school/edit/{id}", "Console\SchoolPage@showEditInfoModal");
            Route::post("school/edit/{id}", "Console\SchoolPage@processEdit");
            Route::get("school/purchase/{id}", "Console\SchoolPage@showPurchaseModal");
            Route::post("school/purchase/{id}", "Console\SchoolPage@processPurchase");

    // Log
        Route::get("log", "Console\LogPage@showLogPage")
        ->name("console/log");
});

/*
|--------------------------------------------------------------------------
| Client Routes
|--------------------------------------------------------------------------
|
*/
Route::get("", function() { 
    return view("template.notice", [
        "title" => "Contact Us",
        "secTitle" => "For use this application"
        ]);
});

Route::get("expired", function() { 
    return view("template.notice", [
        "title" => "Plan expired",
        "secTitle" => "Your school plan has expired"
        ]);
})->name("expired");

Route::prefix('{code}')->group(function () {

    // User Auth
        Route::get("", "Client\UserAuth@showLoginPage")
            ->name("student/login");
        Route::post("", "Client\UserAuth@processStudent");
        Route::get("teacher", "Client\UserAuth@showTeacherLoginPage")
            ->name("teacher/login");
        Route::post("teacher", "Client\UserAuth@processTeacher");
        Route::get("logout", "Client\UserAuth@processLogout")
            ->name("logout");

    Route::prefix("student")->middleware(['web', 'webStudent'])->group(function () {

        // Dashboard
            Route::get("dashboard", "Client\StudentDashboard@showStudentDashboardPage")
                ->name('student/dashboard');
            Route::post("dashboard", "Client\StudentDashboard@processEdit");
                
            Route::get("teacher", "Client\StudentTeacher@showStudentTeacherPage")
            ->name('student/teacher');

            Route::get("attitude/violation", "Client\Attitude\StudentViolation@getPage")
            ->name('student/attitude/violation');
            Route::get("attitude/violation/export", "Client\Attitude\StudentViolation@exportStudentViolation");

            Route::get("attitude/achievement", "Client\Attitude\StudentAchievement@getPage")
            ->name('student/attitude/achievement');

            Route::get("mading/explore", "Client\Mading\StudentExplore@showExplore")
            ->name("student/mading/explore");
                Route::get("mading/explore/read/{id}", "Client\Mading\StudentExplore@showRead");
                Route::post("mading/explore/read/{id}", "Client\Mading\StudentExplore@saveComment");
    });

    Route::prefix("teacher")->middleware(['web', 'webTeacher'])->group(function () {

        // Dashboard
            Route::get("dashboard", "Client\TeacherDashboard@showTeacherDashboardPage")
                ->name('teacher/dashboard');

        // Class
            Route::get("class", "Client\TeacherClass@showTeacherClassPage")
                ->name('teacher/class');
                Route::post("class/add", "Client\TeacherClass@processAdd");
                Route::get("class/delete/{id}", "Client\TeacherClass@processDelete");
                Route::get("class/edit/{id}", "Client\TeacherClass@showEditModal");
                Route::post("class/edit/{id}", "Client\TeacherClass@processEdit");

        // Student
            Route::get("student", "Client\TeacherStudent@showTeacherStudentPage")
            ->name('teacher/student');
                Route::get("student/add", "Client\TeacherStudent@showAddModal");
                Route::post("student/add", "Client\TeacherStudent@processAdd");
                Route::get("student/delete/{id}", "Client\TeacherStudent@processDelete");
                Route::get("student/edit/{id}", "Client\TeacherStudent@showEditModal");
                Route::post("student/edit/{id}", "Client\TeacherStudent@processEdit");
                Route::get("student/export", "Client\TeacherStudent@exportStudent");
                
        // Teacher
            Route::get("admin", "Client\TeacherTeacher@showTeacherPage")
            ->name('teacher/teacher');
                Route::get("admin/add", "Client\TeacherTeacher@showAddModal");
                Route::post("admin/add", "Client\TeacherTeacher@processAdd");
                Route::get("admin/delete/{id}", "Client\TeacherTeacher@processDelete");
                Route::get("admin/edit/{id}", "Client\TeacherTeacher@showEditModal");
                Route::post("admin/edit/{id}", "Client\TeacherTeacher@processEdit");

        // Information
            Route::get("information", "Client\TeacherInformation@showInformationPage")
            ->name('teacher/information');
            Route::post("information", "Client\TeacherInformation@processEdit");

            /*
            |--------------------------------------------------------------------------
            | Client Routes
            |--------------------------------------------------------------------------
            |
            */
            // Manage
                Route::get("mading/manage", "Client\Mading\TeacherManage@showManage")
                ->name("teacher/mading/manage");
                    Route::get("mading/manage/add", "Client\Mading\TeacherManage@showAddContent");
                    Route::get("mading/manage/edit/{id}", "Client\Mading\TeacherManage@showEditContent");
                    Route::post("mading/manage/add", "Client\Mading\TeacherManage@saveAddContent");
                    Route::post("mading/manage/edit/{id}", "Client\Mading\TeacherManage@saveEditContent");
                    Route::get("mading/manage/delete/{id}", "Client\Mading\TeacherManage@processDelete");

            // Explore
                Route::get("mading/explore", "Client\Mading\TeacherExplore@showExplore")
                ->name("teacher/mading/explore");
                    Route::get("mading/explore/read/{id}", "Client\Mading\TeacherExplore@showRead");
                    Route::post("mading/explore/read/{id}", "Client\Mading\TeacherExplore@saveComment");
                    Route::get("mading/explore/read/{id}/dComment/{id2}", "Client\Mading\TeacherExplore@deleteComment");

            // Rule
                Route::get("attitude/rule", "Client\Attitude\TeacherRule@showTeacherRulePage")
                ->name('teacher/attitude/rule');
                    Route::get("attitude/rule/add", "Client\Attitude\TeacherRule@showAddModal");
                    Route::post("attitude/rule/add", "Client\Attitude\TeacherRule@processAdd");
                    Route::get("attitude/rule/delete/{id}", "Client\Attitude\TeacherRule@processDelete");
                    Route::get("attitude/rule/edit/{id}", "Client\Attitude\TeacherRule@showEditModal");
                    Route::post("attitude/rule/edit/{id}", "Client\Attitude\TeacherRule@processEdit");

            // Violation
                Route::get("attitude/violation", "Client\Attitude\TeacherViolation@showSelectStudentPage")
                ->name('teacher/attitude/violation');
                    Route::get("attitude/violation/getStudent/{id}", "Client\Attitude\TeacherViolation@getStudent");
                    Route::get("attitude/violation/single/{id}", "Client\Attitude\TeacherViolation@getSingle");
                    Route::get("attitude/violation/single/{id}/export", "Client\Attitude\TeacherViolation@exportStudentViolation");
                    Route::get("attitude/violation/single/{id}/add", "Client\Attitude\TeacherViolation@showAddModal");
                    Route::post("attitude/violation/single/{id}/add", "Client\Attitude\TeacherViolation@processAdd");
                    Route::get("attitude/violation/single/{id}/delete/{idR}", "Client\Attitude\TeacherViolation@processDelete");
                    Route::get("attitude/violation/single/{id}/edit/{idR}", "Client\Attitude\TeacherViolation@showEditModal");
                    Route::post("attitude/violation/single/{id}/edit/{idR}", "Client\Attitude\TeacherViolation@processEdit");      
                    Route::get("attitude/violation/class/{id}", "Client\Attitude\TeacherViolation@showClass");     
                    Route::get("attitude/violation/class/{id}/export", "Client\Attitude\TeacherViolation@exportClass");     
            
            // Achievement
                Route::get("attitude/achievement", "Client\Attitude\TeacherAchievement@showSelectStudentPage")
                    ->name('teacher/attitude/achievement');
                    Route::get("attitude/achievement/getStudent/{id}", "Client\Attitude\TeacherAchievement@getStudent");
                    Route::get("attitude/achievement/single/{id}", "Client\Attitude\TeacherAchievement@getSingle");
                    Route::get("attitude/achievement/single/{id}/add", "Client\Attitude\TeacherAchievement@showAddModal");
                    Route::post("attitude/achievement/single/{id}/add", "Client\Attitude\TeacherAchievement@processAdd");
                    Route::get("attitude/achievement/single/{id}/delete/{idR}", "Client\Attitude\TeacherAchievement@processDelete");
                    Route::get("attitude/achievement/single/{id}/edit/{idR}", "Client\Attitude\TeacherAchievement@showEditModal");
                    Route::post("attitude/achievement/single/{id}/edit/{idR}", "Client\Attitude\TeacherAchievement@processEdit");
            
            // Statistic
                Route::get("attitude/statistics", "Client\Attitude\TeacherStatistics@showPage")
                    ->name("teacher/attitude/statistics");
    });

});
