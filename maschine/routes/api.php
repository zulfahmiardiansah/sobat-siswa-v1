<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

// Login
    Route::post("login", "Api\Auth@Login");

// Logout
    Route::get("logout", "Api\Auth@logout");

// Level One
    Route::middleware(['api/levelOne'])->group(function () { 

        // School
            Route::prefix("school")->group(function () {
                // Get School Data
                    Route::get("", "Api\SchoolRes@getData");
            });

        // Class
            Route::prefix("class")->group(function () {
                // Get Class Data
                    Route::get("", "Api\ClassRes@getDatas");
                // Get Single Class Data
                    Route::get("{id}", "Api\ClassRes@getData");
            });

        // Student
            Route::prefix("student")->group(function () {
                // Get All Data
                    Route::get("all/{classId}", "Api\StudentRes@getDatas");
                // Get Single
                    Route::get("{id}", "Api\StudentRes@getData");
            });
            
    });

